<?php

/**
 * @file
 * Show TRUE if timestamp is odd, returns FALSE if even
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Rolique test content type'),
  'description' => t('Rolique test content type'),
  'render callback' => '_rolique_test_content_type_render',
  'required context' => new ctools_context_required(t('Rolique test'), 'Rolique'),
  'category' => array(t('Rolique Text')),
  'single' => TRUE,
);

/**
 * Render the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function _rolique_test_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  if (isset($context->data)) {
    $context_data = $context->data ? 'TRUE' : 'FALSE';
    $block->content = $context_data;
  }
  return $block;
}
