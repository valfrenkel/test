<?php

/**
 * @file
 * Plugin to provide access if timestamp is odd
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('rolique test access'),
  'description' => t('rolique test access'),
  'callback' => '_rolique_test_access_check',
  'required context' => new ctools_context_required(t('Rolique test'), 'Rolique'),
  'settings form' => '_rolique_test_access_settings',
);


/**
 * Check for access.
 */
function _rolique_test_access_check($conf, $contexts) {
  if($contexts && $contexts->type == 'Rolique') {
    return $contexts->data;
  }
}

/**
 * Provide a summary description.
 */
function _rolique_test_access_settings($form, &$form_state, $conf) {
  return $form;
}


