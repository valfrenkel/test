<?php

/**
 * @file
 * Rolique ctools context type plugin that return TRUE if timestamp is odd, returns FALSE if even.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('rolique test'),
  'description' => t('rolique test'),
  'context' => '_rolique_test_context',
  'keyword' => 'rolique_test',
  'context name' => 'Rolique',
  'no ui' => FALSE,
);

/**
 * Create a context.
 *
 * @param $empty
 *   If true, just return an empty context.
 * @param $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param $conf
 *   TRUE if the $data is coming from admin configuration, FALSE if it's from a URL arg.
 *
 * @return
 *   a Context object.
 */
function _rolique_test_context($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('Rolique');
  $context->type = 'Rolique';
  $context->data = _rolique_test_is_odd_timestamp();
  $context->title = t('rolique test');
  return $context;
}
