<?php
/**
 * @file
 * rolique_test_feature.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function rolique_test_feature_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'rolique_test';
  $page->task = 'page';
  $page->admin_title = 'Rolique test';
  $page->admin_description = 'Rolique test';
  $page->path = 'test2';
  $page->access = array(
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_rolique_test_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'rolique_test';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Rolique test',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'rolique test',
        'keyword' => 'rolique_test',
        'name' => 'rolique_test',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'threecol_25_50_25_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '122a4228-1223-4567-8aa7-ee94cd615a2b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-93facea1-f617-41a1-85cf-4b99d8d86df8';
    $pane->panel = 'middle';
    $pane->type = 'rolique_test';
    $pane->subtype = 'rolique_test';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '93facea1-f617-41a1-85cf-4b99d8d86df8';
    $display->content['new-93facea1-f617-41a1-85cf-4b99d8d86df8'] = $pane;
    $display->panels['middle'][0] = 'new-93facea1-f617-41a1-85cf-4b99d8d86df8';
    $pane = new stdClass();
    $pane->pid = 'new-ba572b4f-8963-4d62-bf42-c6fdeca8a14b';
    $pane->panel = 'middle';
    $pane->type = 'rolique_test';
    $pane->subtype = 'rolique_test';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'rolique_test',
          'settings' => NULL,
          'context' => 'context_rolique_test_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ba572b4f-8963-4d62-bf42-c6fdeca8a14b';
    $display->content['new-ba572b4f-8963-4d62-bf42-c6fdeca8a14b'] = $pane;
    $display->panels['middle'][1] = 'new-ba572b4f-8963-4d62-bf42-c6fdeca8a14b';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['rolique_test'] = $page;

  return $pages;

}
