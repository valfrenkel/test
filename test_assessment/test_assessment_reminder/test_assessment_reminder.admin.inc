<?php

/**
 * Admin system settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 *   Form API render array.
 */
function test_assessment_reminder_emails_admin_form($form, $form_state) {
  $form = array();
  $form['#attributes'] = array('enctype'  => "multipart/form-data");

  $settings = array(
    1 => array('title' => t('First reminder email defaults'), 'days' => 7),
    2 => array('title' => t('Second reminder email defaults'), 'days' => 2),
    3 => array('title' => t('Third reminder email defaults'), 'days' => 1),
  );
  for ($i = 1; $i <= 3; $i++) {
    $form['test_assess_email_remind_' . $i] = array(
      '#type' => 'fieldset',
      '#title' => $settings[$i]['title'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['test_assess_email_remind_' . $i]['test_assess_email_remind_' . $i . '_days'] = array(
      '#type' => 'textfield',
      '#title' => t('Days before closing'),
      '#default_value' => variable_get('test_assess_email_remind_' . $i . '_days', $settings[$i]['days']),
      '#size' => 60,
      '#element_validate' => array('element_validate_number'),
    );

    $default_subject = variable_get('test_assess_email_remind_' . $i . '_sub', 'TEST Survey reminder for [assessment:org_name]');
    $form['test_assess_email_remind_' . $i]['test_assess_email_remind_' . $i . '_sub'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $default_subject,
      '#size' => 60,
    );

    $info = ($settings[$i]['days'] > 1) ? t('There are') : t('There is');
    $days = $settings[$i]['days'];
    $days_suffix = ($settings[$i]['days'] > 1) ? 's' : '';
    $default_body = variable_get('test_assess_email_remind_' . $i . '_body', array(
      'format' => 'full_html',
      'value' => "Hello [assessment:recipient]
<p>$info $days day$days_suffix left to complete the following survey</p>
<p>[site:url]surveys/email/[assessment:email_code]</p>
<p>Thank you, <br/> Investors in People.</p>",
    ));
    $form['test_assess_email_remind_' . $i]['test_assess_email_remind_' . $i . '_body'] = array(
      '#type' => 'text_format',
      '#format' => $default_body['format'],
      '#title' => t('Body'),
      '#default_value' => $default_body['value'],
      '#size' => 60,
    );
  }

  // Add token help section
  $form['token_help'] = array(
   '#title' => t('Replacement patterns'),
   '#type' => 'fieldset',
   '#collapsible' => TRUE,
   '#collapsed' => TRUE,
  );
  $form['token_help']['help'] = array(
   '#theme' => 'token_tree',
   '#token_types' => array('site', 'node', 'assessment'),
   '#global_types' => TRUE,
   '#click_insert' => TRUE,
  );
  return system_settings_form($form);
}


/**
 * Submission handler for admin system settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function test_assessment_reminder_emails_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values as $value_key => $value) {
    if (substr($value_key, 0, 24) == 'test_assess_email_remind_' &&  is_array($value)) {
      foreach ($value as $var_key => $var) {
        variable_set($var_key, $var);
        // Update field instance
        $field_name = substr_replace($var_key, 'field_assess', 0, 16);
        // Fetch an instance info array.
        $instance_info = field_info_instance('node', $field_name, ASSESSMENT_NODE_TYPE);
        if (isset($instance_info)) {
          if (is_array($var)) {
            if (isset($instance_info['default_value'][0]['value']) && $instance_info['default_value'][0]['format']) {
              $instance_info['default_value'][0] = $var;
            }
            else {
              $instance_info['default_value'][0] = $var['value'];
            }
          }
          else {
            $instance_info['default_value'][0]['value'] = $var;
          }

          // Write the changed definition back.
          field_update_instance($instance_info);
        }
      }
    }
  }

  drupal_set_message(t('The settings have been updated.'));
}