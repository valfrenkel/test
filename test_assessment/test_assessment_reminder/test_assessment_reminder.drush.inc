<?php
/**
 * @file
 * Drush helper commands for TEST assessment email reminders.
 */

/**
 * Implements hook_drush_command().
 *
 * @return array
 *   An associative array describing commands.
 *
 * @see drush_parse_command()
 */
function test_assessment_reminder_drush_command() {
  $items = array();

  $items['test-assessments-send-reminder-email'] = array(
    'description' => dt('Sends reminder emails to Survey recipients'),
    'callback' => 'test_assessment_reminder_send_reminder',
    'drupal dependencies' => array('test_assessment_reminder'),
    'aliases' => array('test-reminder-emails'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(
      'send_count' => 'The number of emails to be send per one drush run.',
    ),
    'examples' => array(
      'drush test-reminder-emails --send_count=10' => 'Send 10 reminders emails.',
    ),
  );

  return $items;
}

/**
 * Implements sending reminder emails to assessment recipients
 */
function test_assessment_reminder_send_reminder() {
  $options = array(
    1 => dt('First reminder'),
    2 => dt('Second reminder'),
    3 => dt('Third reminder'),
  );
  $messages = array();
  $success_message = array();
  $send_count = drush_get_option('send_count', 500);

  // Get flags '-v, --verbose'
  $verbose = drush_get_context('DRUSH_VERBOSE');

  // Gets all assessments which are ready for email sending
  $i = 1;
  for ($reminder = 1; $reminder <= 3; $reminder++) {
    $assessment_count = 0;
    $assessments_query = db_select('node', 'n');
    $assessments_query->innerJoin('field_data_field_survey_state', 'state', 'n.nid = state.entity_id');
    $assessments_query->innerJoin('field_data_field_end_date', 'edate', 'n.nid = edate.entity_id');
    $assessments_query->innerJoin('field_data_field_assess_remind_' . $reminder . '_date', 'days', 'n.nid = days.entity_id');
    $assessments_query->fields('n', array('nid'));
    $assessments_query->condition('n.type', ASSESSMENT_NODE_TYPE)
      ->condition('days.field_assess_remind_' . $reminder . '_date_value', date('Y-m-j 00:00:00'), '<=')
      ->condition('state.field_survey_state_value', 'active');
    // only send reminder emails when the end date is in the future. This is to
    // cover the scenario when we set the End Date to today - to close the survey
    // without "AND (edate.field_end_date_value > NOW())" - we could potentially
    // send reminders before the survey is automatically closed
    //$assessments_query->where('TO_DAYS(edate.field_end_date_value) - TO_DAYS(NOW()) <= days.field_assess_remind_' . $reminder . '_days_value AND (edate.field_end_date_value > NOW())');

    $assessment_result = $assessments_query->execute();
    $assessment_count = $assessments_query->countQuery()->execute()->fetchField();

    if ($assessment_result && $assessment_count > 0) {
      while($record = $assessment_result->fetchAssoc()) {
        $assessment = node_load($record['nid']);

        // Gets test emails for the assessment
        $email_query = db_select('test_email', 'email');
        $email_query->fields('email', array('eid', 'email'));
        $email_query->condition('email.aid', $assessment->nid)
          ->condition('email.completed', 0)
          ->condition(
            db_or()
              ->condition('email.reminder_' . $reminder . '_sent', NULL, 'IS NULL')
              ->condition('email.reminder_' . $reminder . '_sent', 0)
          );

        $email_count = $email_query->countQuery()->execute()->fetchField();
        $email_result = $email_query->execute();
        $j = 0;
        if ($email_result && $email_count > 0) {
          // Get the email don't send checkbox
          $reminder_dont_send = '';
          $reminder_dont_send = field_get_items('node', $assessment, 'field_assess_remind_' . $reminder . '_dont_send');
          if (!$reminder_dont_send[0]['value']) {
            // Get the email subject
            $reminder_subj = '';
            $reminder_subj_field = field_get_items('node', $assessment, 'field_assess_remind_' . $reminder . '_sub');
            if (isset($reminder_subj_field) && isset($reminder_subj_field[0]['value'])) {
              $reminder_subj = $reminder_subj_field[0]['value'];
            }
            // Get the email body
            $reminder_body = '';
            $reminder_body_field = field_get_items('node', $assessment, 'field_assess_remind_' . $reminder . '_body');
            if (isset($reminder_body_field) && isset($reminder_body_field[0]['value'])) {
              $reminder_body = $reminder_body_field[0]['value'];
            }

            // Check if values of the subject and the body aren't empty
            if (empty($reminder_subj) || empty($reminder_body)) {
              $message = dt('The email subject or body are empty for !reminder of the Survey !nid', array(
                '!reminder' => $options[$reminder],
                '!nid' => $assessment->nid,
              ));
              watchdog('test_assessment_reminder', $message);
            }
            else {
              while($email_record = $email_result->fetchAssoc()) {
                $email = entity_metadata_wrapper('test_email', $email_record['eid']);
                // Replaces tokens by their values in email content
                $data = array();
                $data['assessment_id'] = $assessment->nid;
                $code = $email->code->value();
                $data['code'] = (isset($code)) ? $code : '';
                $recipient = $email->recipient->value();
                $data['recipient'] = (isset($recipient)) ? $recipient : '';
                $recipient_lastname = $email->recipient_lastname->value();
                $data['recipient_lastname'] = (isset($recipient_lastname)) ? $recipient_lastname : '';

                $params = array(
                  'subject' => token_replace($reminder_subj, $data),
                  'body' => token_replace($reminder_body, $data),
                );

                $language = language_default();
                $recipient_email = $email->email->value();

                //send mail via mandrill system
                // check if sending via mandrill has been disabled
                if (variable_get('test_mandrill_send', FALSE)) {
                  // Generate fullname for recipient
                  $recipient_name = $data['recipient'];
                  if (isset($data['recipient_lastname']) && !empty($data['recipient_lastname'])) {
                    $recipient_name .= ' ' . $data['recipient_lastname'];
                  }

                  $send_to = array(
                    array(
                      'email' => $recipient_email,
                      'name' => $recipient_name,
                      'type' => 'to'
                    )
                  );
                  // check if we should redirect mail to a testing address
                  if (variable_get('test_mandrill_dev_redirect', TRUE)) {
                    $send_to[0]['email'] = variable_get('test_mandrill_dev_redirect_address', "admin@bigbluedoor.net");
                  }

                  $org_name = _test_assessment_get_organisation_logo($assessment->nid);
                  if(isset($org_name) && $org_name != FALSE) {
                    $org_image = '<img src='.$org_name.' alt="organisation logo" width="80" height="90" />';
                  }
                  else {$org_image = '';}


                  // Set up the field reminder_sent for the current test_email
                  if (_test_assessment_mandrill_send_mail($params['subject'], $params['body'], $send_to, $org_image)) {
                    // Set up the field reminder_sent for the current test_email
                    $field_name = 'reminder_' . $reminder . '_sent';
                    $email->{$field_name} = time();
                    $email->save();

                    $i++;$j++;
                    $success_message[$reminder][$assessment->nid] = $j;

                    if ($send_count < $i) {
                      break 3;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      $message = dt('There aren\'t any Survey for sending for the !reminder', array('!reminder' => $options[$reminder]));
      if (!$verbose) {
        $messages[] = $message;
      }
      else {
        watchdog('test_assessment_reminder', $message);
      }
    }
  }

  if ($success_message) {
    foreach ($success_message as $reminder => $assessments) {
      foreach ($assessments as $aid => $count) {
        $message = dt('Sent !count !reminder emails for Survey !nid', array(
          '!count' => $count,
          '!reminder' => $options[$reminder],
          '!nid' => $aid,
        ));
        watchdog('test_assessment_reminder', $message);
        $messages[] = $message;
      }
    }
  }

  $messages = implode("\n", $messages);
  print($messages . "\n");
}