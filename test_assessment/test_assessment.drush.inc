<?php

/*
 * Implements of hook_drush_command().
 */
function test_assessment_drush_command() {
  $items = array();
  $items['test-assessments-set-active-send-emails'] = array(
    'description' => 'Set active state and send messasage',
    'callback' => '_test_assessments_set_active_send_emails',
    'aliases' => array('test-active-emails'),
    'options' => array(
      'send_count' => 'The number of emails to be send per one drush run.',
    ),
  );
  $items['test-assessments-close'] = array(
    'description' => 'Set "closed" state to survey',
    'callback' => '_test_assessments_set_closed',
  );
  $items['test-assessments-submission-sync-egroup'] = array(
    'description' => 'Sync values of the field "submission_email_group" for submissions with values of the "egroup" for test_emails (due to addition new functionality)',
    'callback' => '_test_assessments_submission_sync_egroup',
  );
  $items['test-assessments-add-lang'] = array(
    'description' => 'Set langcode and Questionnaire to survey. We need it set up language functionality for existing surveys',
    'callback' => '_test_assessments_add_lang',
  );
  $items['test-add-default-reminders'] = array(
    'description' => 'Set default reminders to excisting surveys.',
    'callback' => '_test_add_default_reminders',
  );
  return $items;
}

function _test_assessments_set_active_send_emails() {

  ///Select all Assessment nodes where state = 'Ready' and start_date >= NOW.
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'assessment')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_end_date', 'value', date("Y-m-d 00:00:00"), '>=')
    ->fieldCondition('field_start_date', 'value', date("Y-m-d 00:00:00"), '<=')
    ->fieldCondition('field_survey_state', 'value', 'ready', '=')
    ->execute();
  $i = $j = 0;
  if(isset($entities['node'])) {
    foreach ($entities['node'] as $key => $value) {
      //Change field_survey_state field to active for necessary nodes.
      $nodewrapper = entity_metadata_wrapper('node', node_load($key));
      $nodewrapper->field_survey_state = 'active';
      $nodewrapper->save();
      //iterator for drush print
      $i++;
      //log to watchdog log "Set Assessment AID to Active"
      watchdog('test_assessment', 'Set Survey %aid to Active', array('%aid' => $key) , WATCHDOG_INFO);

      //generate emails which should be sent for current assessment
      //$sent_mails = _test_assessments_generate_email($key);
    }
  }
  $query = new EntityFieldQuery();
  $active_entities = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'assessment')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_survey_state', 'value', 'active', '=')
    ->addTag('joinwithemails')
    ->execute();
  
  if(isset($active_entities['node'])) {
    module_load_include('inc', 'test_assessment', 'test_assessment.common');
    $send_count = drush_get_option('send_count', 500);
    foreach ($active_entities['node'] as $key => $value) {
      $result = db_select('test_email', 'email')
        ->fields('email', array('eid', 'email', 'recipient', 'recipient_lastname'))
        ->condition('email.aid', $key,'=')
        ->condition('email.sent', 0,'=')
        ->execute();
      while(($record = $result->fetchAssoc()) && ($j<$send_count)) {
        $j++;
        _test_assessment_emails_sending($key, $record);
      }
      watchdog('test_assessment', 'Send %count for %aid', array('%aid' => $key, '%count' => $j) , WATCHDOG_INFO);
      if ($send_count < $j) {
        break;
      }
    }
  }
  print(dt("Survey State set to Active for $i nodes \n"));
  print(dt("Sent $j survey emails \n"));
}

/*
 * Implements of hook_query_TAG_alter().
 */

function test_assessment_query_joinwithemails_alter(QueryAlterableInterface $query) {
  $query->innerJoin('test_email', 'test_email', 'test_email.aid = node.nid');
  $query->condition('test_email.sent', 0, '=');
}


/**
 * Automatically Sets Assessments to "closed" status
 */
function _test_assessments_set_closed() {
  ///Select all Assessment nodes where state NOT IN ('closed', 'archived')" and end_date <= NOW.
  $close_date_timestamp = strtotime('-1 day');

  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'assessment')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_end_date', 'value', date("Y-m-d 00:00:00", $close_date_timestamp), '<=')
    ->fieldCondition('field_survey_state', 'value', array('closed', 'archived'), 'NOT IN')
    ->execute();
  $i = 0;
  if(isset($entities['node'])) {
    foreach ($entities['node'] as $key => $value) {
      //Change field_survey_state field to closed.
      $nodewrapper = entity_metadata_wrapper('node', node_load($key));
      $nodewrapper->field_survey_state = 'closed';
      $nodewrapper->save();
      //iterator for drush print
      $i++;
      //log to watchdog log "Set Assessment AID to Closed"
      watchdog('test_assessment', 'Set Survey %aid to Closed', array('%aid' => $key) , WATCHDOG_INFO);
    }
  }
  print(dt("Survey State set to Closed for $i nodes \n"));
}

/**
 * Syncs values of the field "submission_email_group" for submissions with values of the "egroup" for test_emails
 */
function _test_assessments_submission_sync_egroup() {
  // Select all submissions where submission method is equal to 'email'.
  $query= db_select('entityform', 'entityform');
  $query->innerJoin('field_data_field_submission_code', 'code', 'entityform.entityform_id = code.entity_id');
  $query->innerJoin('field_data_field_submission_method', 'method', 'entityform.entityform_id = method.entity_id');
  $query->innerJoin('test_email', 'email', 'code.field_submission_code_value = email.code');
  $query->fields('entityform', array('entityform_id'))
    ->fields('code', array('field_submission_code_value'))
    ->fields('email', array('egroup'))
    ->condition('method.field_submission_method_value', 'email')
    ->condition('email.egroup', '', '<>')
    ->isNotNull('email.egroup');

  $result = $query->execute()->fetchAll();

  $i = 0;
  if (isset($result) && !empty($result)) {
    foreach ($result as $entityform) {
      $entityform_wrap = entity_metadata_wrapper('entityform', entityform_load($entityform->entityform_id));
      $entityform_wrap->field_submission_email_group = $entityform->egroup;
      $entityform_wrap->save();

      $i++;
    }
  }

  print(dt("Updated $i submissions (added value for field 'field_submission_email_group') \n"));
}

/**
 * Set language code and fill new questionnaire field
 */
function _test_assessments_add_lang() {
  ///Select all Assessment nodes where field_questionnaire_lang not set
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'assessment')
    ->propertyCondition('status', 1)
    ->execute();
  $i = 0;
  if(isset($entities['node'])) {
    foreach ($entities['node'] as $key => $value) {
      $nodewrapper = entity_metadata_wrapper('node', node_load($key));
      $field_questionnaire_lang = $nodewrapper->field_questionnaire_lang->value();
      if (!isset($field_questionnaire_lang)) {
        $nodewrapper->field_langcode = 'en';
        $entity_form = $nodewrapper->field_questionnaire->value();
        $entity_form_type = $entity_form->type;
        if (isset($entity_form_type)) {
          $entity_form_type_array = explode('_questions', $entity_form_type);
          $nodewrapper->field_questionnaire_lang = ucfirst($entity_form_type_array[0]).'_en';
        }
        $nodewrapper->save();
        //iterator for drush print
        $i++;
      }
    }
  }
  print(dt("Fields updated for $i surveys \n"));
}

function _test_add_default_reminders () {
  ///Select all Assessment nodes where field_questionnaire_lang not set
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'assessment')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_survey_state', 'value', array('ready', 'draft', 'active'), 'IN')
    ->execute();
  $j = 0;
  if(isset($entities['node'])) {
    foreach ($entities['node'] as $key => $value) {
      $nodewrapper = entity_metadata_wrapper('node', node_load($key));
      $field_lang = $nodewrapper->field_langcode->value();
      for ($i = 1; $i <= 3; $i++) {
        $field_assess_remind_sub = 'field_assess_remind_'.$i.'_sub';
        $field_assess_remind_body = 'field_assess_remind_'.$i.'_body';
        //$nodewrapper->{$field_assess_remind_sub}
        $reminder_subject = $nodewrapper->{$field_assess_remind_sub}->value();
        $reminder_body = $nodewrapper->{$field_assess_remind_body}->value();
        $reminder_body = $reminder_body['value'];
        
        if(!isset($reminder_subject) || empty($reminder_subject)) {
          $nodewrapper->{$field_assess_remind_sub}= i18n_variable_get('test_assess_email_remind_' . $i . '_sub', $field_lang);
          $j++;
        }
        if(!isset($reminder_body) || empty($reminder_body)) {
          $default_body_text = i18n_variable_get('test_assess_email_remind_' . $i . '_body', $field_lang);
          $nodewrapper->{$field_assess_remind_body}->set(array('value' => $default_body_text['value']));
          $j++;
        }
      }
      $nodewrapper->save();
    }
  }
  print(dt("Fields updated for $j surveys \n"));
}