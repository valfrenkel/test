<?php

/**
 * Admin system settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 *   Form API render array.
 */
function test_assessment_mandrill_admin_form($form, $form_state) {
  //Add feild to insert key
  $default_key = variable_get('test_mandrill_key', '');
  $form['test_mandrill_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandrill API Key'),
    '#default_value' => $default_key,
    '#description' => 'Create or grab your API key from the <a href="https://mandrillapp.com/settings/index" target="_blank">Mandrill settings</a>.',
    '#size' => 60,
  );

  $form['test_mandrill_send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Mandrill sending'),
    '#default_value' => variable_get('test_mandrill_send', FALSE),
    '#description' => 'If unchecked - no email will be sent via mandrill - not even Dev redirects (see below)',
  );

  $form['test_mandrill_dev_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect Mandrill email to dev email'),
    '#default_value' => variable_get('test_mandrill_dev_redirect', TRUE),
    '#description' => 'If checked - email will be redirected to a test address',
  );

  $form['test_mandrill_dev_redirect_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect Mandrill email to dev email address'),
    '#default_value' => variable_get('test_mandrill_dev_redirect_address', "admin@bigbluedoor.net"),
    '#description' => 'Email address to redirect all Surveys Mandrill email to',
    '#size' => 60,
  );
  
  return system_settings_form($form);
}