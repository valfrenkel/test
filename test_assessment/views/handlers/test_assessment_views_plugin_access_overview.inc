<?php
/**
 * @file
 * Definition of test_assessment_views_plugin_access_overview.
 */

/**
 * Access plugin that provides Sutton Trust access control for different roles.
 *
 * This is needed because it's not just based on role or permission - we need
 * to check two permissions, and views (actually, Drupal) doesn't allow for an
 * OR in the menu callback.
 *
 * @ingroup views_access_plugins
 */
class test_assessment_views_plugin_access_overview extends views_plugin_access {

  /**
   * Determine if the current user has access or not.
   *
   * @param $account
   *   User account.
   * @return bool
   *   TRUE or FALSE.
   */
  function access($account) {
    return test_assessment_admin_overview_access($account);
  }

  /**
   * Determine the access callback and arguments.
   *
   * This information will be embedded in the menu in order to reduce
   * performance hits during menu item access testing, which happens
   * a lot.
   *
   * @return array
   *   the first item should be the function to call, and the second item should
   *   be an array of arguments. The first item may also be TRUE (bool only)
   *   which will indicate no access control.)
   */
  function get_access_callback() {
    return array('test_assessment_admin_overview_access');
  }

  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    return t('TEST Survey - Custom access callback');
  }
}
