<?php
/**
 * @file
 * Definition of test_assessment_views_plugin_access_overview.
 */

/**
 * Access plugin that provides Sutton Trust access control for different roles.
 *
 * This is needed because it's not just based on role or permission - we need
 * to check two permissions, and views (actually, Drupal) doesn't allow for an
 * OR in the menu callback.
 *
 * @ingroup views_access_plugins
 */
class test_assessment_views_plugin_access_display extends views_plugin_access {

  /**
   * Determine if the current user has access or not.
   *
   * @param $account
   *   User account.
   * @return bool
   *   TRUE or FALSE.
   */
  function access($account) {
    $roles = array_filter($this->options['role']);
    $role_profile = (isset($this->options['role_profile'])) ? array_filter($this->options['role_profile']) : NULL;
    return test_assessment_views_check_access_display($roles, $role_profile, $account);
  }

  /**
   * Determine the access callback and arguments.
   *
   * This information will be embedded in the menu in order to reduce
   * performance hits during menu item access testing, which happens
   * a lot.
   *
   * @return array
   *   the first item should be the function to call, and the second item should
   *   be an array of arguments. The first item may also be TRUE (bool only)
   *   which will indicate no access control.)
   */
  function get_access_callback() {
    $roles = array_filter($this->options['role']);
    $role_profile = (isset($this->options['role_profile'])) ? array_filter($this->options['role_profile']) : NULL;
    return array('test_assessment_views_check_access_display', array($roles, $role_profile));
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['role'] = array('default' => array());
    $options['role_profile'] = array('default' => array());

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['role'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Role'),
      '#default_value' => $this->options['role'],
      '#options' => array_map('check_plain', views_ui_get_roles()),
      '#description' => t('Only the checked roles will be able to access this display. Note that users with "access all views" can see any view, regardless of role.'),
    );

    $form['role_profile'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Profile'),
      '#default_value' => $this->options['role_profile'],
      '#options' => array_map('check_plain', views_ui_get_roles()),
      '#description' => t('Only for the checked roles this display will be displayed.'),
    );
  }

  function options_validate(&$form, &$form_state) {
    if (!array_filter($form_state['values']['access_options']['role'])) {
      form_error($form['role'], t('You must select at least one role if type is "by role"'));
    }
  }

  function options_submit(&$form, &$form_state) {
    $form_state['values']['access_options']['role'] = array_filter($form_state['values']['access_options']['role']);
    $form_state['values']['access_options']['role_profile'] = array_filter($form_state['values']['access_options']['role_profile']);
  }

  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    return t('TEST Survey - Access for display by role');
  }
}