<?php

/**
 * @file
 * Definition of test_assessment_views_handler_field_custom.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_handler_field_custom
 */
class test_assessment_views_handler_field_custom extends views_handler_field_custom {

  function render($values) {
    module_load_include('inc', 'test_assessment', 'test_assessment.common');
    // Gets all submissions for the assessment
    $subms = 0;
    if (isset($values->nid)) {
      $assessment = node_load($values->nid);
      if ($assessment->type == ASSESSMENT_NODE_TYPE) {
        $subms = _test_assessment_get_existing_submissions($assessment);
        if ($subms > 0) {
          $this->options['alter']['make_link'] = 1;
        }
        else {
          $this->options['alter']['make_link'] = 0;
        }
      }
    }

    // Return the text, so the code never thinks the value is empty.
    return $this->options['alter']['text'];
  }
}