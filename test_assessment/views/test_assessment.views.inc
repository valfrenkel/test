<?php

/**
 * Implements hook_views_data().
 *
 * @see hook_views_data()
 */
function test_assessment_views_data() {
  $table_name = 'test_assessment';
  $data[$table_name]['table']['group'] = t('Global');
  $data[$table_name]['table']['join'] = array(
    // #global is a special flag which let's a table appear all the time.
    '#global' => array(),
  );

  $data[$table_name]['test_nothing'] = array(
    'title' => t('TEST Custom text'),
    'help' => t('Provide custom text with link'),
    'field' => array(
      'handler' => 'test_assessment_views_handler_field_custom',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data().
 */
function test_assessment_views_data_alter(&$data) {
  if (isset($data['entityform_type'])) {
    $data['entityform_type']['data'] = array (
      'title' => t('Additional data'),
      'help' => t('A serialized array of additional data related to this entityform type.'),
      'real field' => 'data',
      'field' => array(
        'handler' => 'views_handler_field_serialized',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
    );
  }
}

/**
 * Implements hook_views_plugins().
 *
 * @return array
 *   An array on the form $plugins['PLUGIN TYPE']['PLUGIN NAME']. The plugin
 *   must be one of row, display, display_extender, style, argument default,
 *   argument validator, access, query, cache, pager, exposed_form or
 *   localization. The plugin name should be prefixed with your module name.
 *   The value for each entry is an associative array.
 *
 * @see hook_views_plugins()
 */
function test_assessment_views_plugins() {
  $plugins = array(
    // display, style, row, argument default, argument validator and access.
    'access' => array(
      'test_assessment_overview' => array(
        'title' => t('TEST Survey - Custom access callback'),
        'help' => t('Access will be restricted by multiple permissions. See test_assessment_admin_overview_access().'),
        'handler' => 'test_assessment_views_plugin_access_overview',
        'uses options' => FALSE,
      ),
      'test_assessment_tier2_access' => array(
        'title' => t('TEST Survey - Access for "Tier2" users'),
        'help' => t('Determines access to Survey for "Tier2" users.'),
        'handler' => 'test_assessment_views_plugin_access_tier2_users',
        'uses options' => TRUE,
      ),
      'test_assessment_display_access' => array(
        'title' => t('TEST Survey - Access for display by role'),
        'help' => t('Determines access to display page by role.'),
        'handler' => 'test_assessment_views_plugin_access_display',
        'uses options' => TRUE,
      ),
    ),
  );
  return $plugins;
}

/**
 * Implements hook_views_query_alter().
 *
 * @param $view
 *   The view object about to be processed.
 * @param $query
 *   An object describing the query.
 */
function test_assessment_views_query_alter(&$view, &$query) {

  if ($view->name == 'manage_assessments' && $view->current_display == 'page') {
    // Delete filter by 'field_survey_state'
    // Implements custom filtration by this field
    if (isset($query->where[1]['conditions']) && is_array($query->where[1]['conditions'])) {
      foreach ($query->where[1]['conditions'] as $key => $value) {
        if (is_string($value['field']) && substr($value['field'], 0, 29) == 'field_data_field_survey_state') {
          unset($query->where[1]['conditions'][$key]);
        }
      }
    }

    // If filter 'State' is selected display only assessments with state from this filter
    if (isset($view->exposed_input['field_survey_state_value']) && $view->exposed_input['field_survey_state_value'] != 'All') {
      $states[$view->exposed_input['field_survey_state_value']] = $view->exposed_input['field_survey_state_value'];
      if (isset($view->exposed_input['field_survey_state_include'])) {
        $states += $view->exposed_input['field_survey_state_include'];
      }
    }
    // If filter 'State' isn't selected display assessments with states 'draft', 'ready', 'active' and 'closed'
    // If checked filter 'include' adds values of the filter
    else {
      $states = array(
        'draft' =>'draft',
        'ready' => 'ready',
        'active' => 'active',
        'closed' => 'closed',
      );
      if (isset($view->exposed_input['field_survey_state_include'])) {
        $states += $view->exposed_input['field_survey_state_include'];
      }
    }

    if (isset($states) && !empty($states)) {
      array_unique($states);
      $query->add_where(1, 'field_data_field_survey_state.field_survey_state_value', $states, 'IN');
    }
  }
}
