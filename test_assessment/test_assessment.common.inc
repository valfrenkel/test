<?php
/**
 * @file
 * Assessments common functions
 */

/**
 * Imports emails from CSV
 *
 * @param $fid
 * @param int $aid
 * @param bool $surveys_state_active
 * @return array|bool|null|void
 */
function _test_assessment_process_emails_csv($fid, $aid = 0, $surveys_state_active = false){
  $emails = file_load($fid);
  ini_set('auto_detect_line_endings', true);
  // We don't have the file to import - stop here
  if (!$emails) return false;

  header('Content-Type: text/html; charset=UTF-8');

  $file = fopen($emails->uri, 'r');
  $header = fgetcsv($file); // first line is a header
  $counter = 0;
  $messages = array();
  $map = _test_email_get_mapping();

  $failed_emails = 0;
  $imported_emails = 0;

  // report line numbers accurately - the first line is the header - so the first row of data is actually row 2
  $counter++;
  while ($row = fgetcsv($file)) {

    $row = array_map("utf8_encode", $row);

    $counter++;
    foreach ($header as $key => $value){
      $source_value = (isset($row[$key]) && !empty($row[$key])) ? $row[$key] : '';
      $values[$value] = $source_value;
    }

    // Check if the 'email' exists (mandatory field 'email')
    if (isset($map['email'])){
      $email_key = $map['email'];
      if (empty($values[$email_key]) || _test_assessment_email_is_valid($values[$email_key]) == FALSE){
        $messages[] = t('Line !line have incorrect email address', array('!line' => $counter));
        $failed_emails++;
        continue;
      }
    } else {
      $email_key = 'email';
      if (empty($values['email']) || _test_assessment_email_is_valid($values['email']) == FALSE){
        $messages[] = t('Line !line have incorrect email address', array('!line' => $counter));
        $failed_emails++;
        continue;
      }
    }

    // Check if the 'Name' exists (mandatory field 'recipient')
    if (isset($map['recipient'])){
      $recipient_key = $map['recipient'];
      if (empty($values[$recipient_key])){
        $messages[] = t('Line !line doesn\'t have the mandatory field @name', array('!line' => $counter, '@name' => $recipient_key));
        $failed_emails++;
        continue;
      }
    } else {
      $recipient_key = 'Name';
      if (empty($values['Name'])){
        $messages[] = t('Line !line doesn\'t have the mandatory field "Name"', array('!line' => $counter));
        $failed_emails++;
        continue;
      }
    }

    // Check if the 'Surname' exists (mandatory field 'recipient_lastname')
    if (isset($map['recipient_lastname'])){
      $recipient_lastname_key = $map['recipient_lastname'];
    } else {
      $recipient_lastname_key = 'Surname';
    }

    // Execute a count query to see if there's other similar values
    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', 'test_email')
      ->propertyCondition('aid', $aid)
      ->propertyCondition('email', $values[$email_key]);

    $count = $query->count()->execute();
    if ($count > 0) {
      $messages[] = t('Line !line have duplicate entry and was not imported', array('!line' => $counter));
      $failed_emails++;
      continue;
    }

    try {
      $recipient = $values[$recipient_key];
      $recipient_lastname = (isset($values[$recipient_lastname_key])) ? $values[$recipient_lastname_key] : '';

      // Build array with values of the fields
      $email_fields = array(
        'aid' => $aid,
        'email' => $values[$email_key],
        'recipient' => $recipient,
        'recipient_lastname' => $recipient_lastname,
      );

      if (isset($map['egroup'])){
        $egroup_key = $map['egroup'];
        if (isset($values[$egroup_key]) && !empty($values[$egroup_key])) {
          $email_fields['egroup'] = $values[$egroup_key];
        }
      } else {
        if (isset($values['Group']) && !empty($values['Group'])) {
          $email_fields['egroup'] = $values['Group'];
        }
      }

      $entity = entity_create('test_email', $email_fields);
      if ($entity){
        // Save new entity
        $entity->save();
        //Emails sending if imported on active state
        if($surveys_state_active) {
          $record = $email_fields;
          $record['eid'] = $entity->eid;
          _test_assessment_emails_sending($aid, $record);
        }
        // Don't need to show successfully imported line
        //$messages[] = t('The line !line has been imported successfully.', array('!line' => $counter));
        $imported_emails++;
      }
    }
    catch(Exception $e) {
      watchdog('test_email', $e->getMessage());
      $messages[] = t('Line !line was not imported.', array('!line' => $counter));
      $failed_emails++;
    }
  }

  // Adds conclusion message
  $messages[] = $imported_emails . ' ' . t('emails imported') . ' - ' . $failed_emails . ' ' . t('failed');

  if (count($messages)){
    return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }
}

/**
 * Helper function - import submissions.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 * @param $node
 *   The $node object.
 *
 * @return array
 */
function _test_assessment_import_submissions($form, &$form_state, $node, $type = 'csv') {
  $values = $form_state['values'];
  $messages = array();
  $entityform_fields = array();
  $entityform_type = $entityform_label = '';

  // Imports submissions
  // Gets question set and the fields of this set
  $questionnaire_field = field_get_items('node', $node, 'field_questionnaire');
  if (isset($questionnaire_field) && isset($questionnaire_field[0]['target_id'])) {
    $questionnaire = entity_load('entityform_type', array($questionnaire_field[0]['target_id']));
    if (isset($questionnaire[$questionnaire_field[0]['target_id']]->type)) {
      $entityform_type = $questionnaire[$questionnaire_field[0]['target_id']]->type;
      $entityform_label = $questionnaire[$questionnaire_field[0]['target_id']]->label;
      $entityform_fields = field_info_instances('entityform', $entityform_type);

      if (!empty($entityform_fields)) {
        $entityform_fields = array_keys($entityform_fields);
      }
    }
  }

  if (!$entityform_type) {
    $messages[] = t('The import is impossible. Please set up "Questionnare" for this survey.');
    watchdog('test_assessment', 'The questionnare hasn\'t been set up yet.');
    return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }

  if (empty($entityform_fields)) {
    $messages[] = t('The import is impossible. Please contact administrator.');
    watchdog('test_assessment', 'The questionnare hasn\'t any fields.');
    return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }

  switch ($type) {
    case 'csv':
      if (isset($values['submissions']) && $values['submissions'] > 0) { // have uploaded file
        // Gets surveys token
        _test_assessment_process_submissions_import_csv($values['submissions'], $node, $entityform_type, $entityform_label, $entityform_fields);
      }
      break;
    case 'pdf':
      if (isset($values['pdfs']) && !empty($values['pdfs'])) { // have uploaded file
        // Gets surveys token
        foreach ($values['pdfs'] as $pdf_fid) {
          _test_assessment_process_submissions_import_pdf($pdf_fid, $node, $entityform_type, $entityform_label, $entityform_fields);
        }
      }
      break;
    case 'paper':
      if (isset($values['paper_submissions']) && $values['paper_submissions'] > 0) { // have uploaded file
        _test_assessment_process_submissions_import_paper($values['paper_submissions'], $node, $entityform_type, $entityform_label, $entityform_fields);
      }
      break;
  }

  return $messages;
}

/**
 * Helper function - import questionnaires submissions from csv.
 *
 * @param $fid
 * @param $aid
 * @param $entityform_type
 * @param $entityform_label
 * @param $header_fields
 *
 * @throws Exception
 * @return array
 */
function _test_assessment_process_submissions_import_csv($fid, $aid, $entityform_type, $entityform_label, $header_fields) {
  global $user;
  $messages = array();

  $file = file_load($fid);
  // Gets associative result array
  try {
    ini_set('auto_detect_line_endings', true);
    $handle = fopen($file->uri, 'r');
    $header = fgetcsv($handle);
    // Check if header is correct
    $incorrect = array();
    foreach ($header as $source_name) {
      if (!in_array($source_name, $header_fields)) {
        $incorrect[] = $source_name;
      }
    }

    if (!empty($incorrect)) {
      $be = (count($incorrect) > 1) ? 'were' : 'was';
      $messages[] = t("'%source_names' %be not found on Questionnaire '%questionnaire_label'. Please check field names and try again. 0 Rows imported.", array('%source_name' => implode(' , ', $incorrect), '%be' => $be, '%questionnaire_label' => $entityform_label));
        return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
    }

    $counter = 0;
    while (($data = fgetcsv($handle)) !== FALSE) {
      $row_break = FALSE;
      $counter++;

      $line = array();
      foreach ($header as $key => $value) {
        $field = field_info_field($value);
        $allowed_values= list_allowed_values($field);
        $data_value = (isset($data[$key])) ? $data[$key] : '';
        // Check if the data of the field is correct
        if (isset($allowed_values) && $data_value) {
          if (array_key_exists($data[$key], $allowed_values)) {
            $line[$value] = $data_value;
          }
          else {
            $messages[] = t("The line !line failed - invalid data '%data' for the field '%value'", array('!line' => $counter, '%data' => $data[$key], '%value' => $value));
            $row_break = TRUE;
          }
        }
        else {
          $line[$value] = $data_value;
        }
      }

      if ($row_break) {
        $messages[] = t("The line !line has not been imported.", array('!line' => $counter));
        continue;
      }

      // Creates entity 'entityform'
      if ($value = array_filter($line)) {
        // ID of the assessment
        $value['aid'] = $aid->nid;

        try {
          // Create submission
          $entityform = entity_create('entityform', array(
            'type' => $entityform_type,
            'created' => time(),
            'changed' => time(),
            'language' => LANGUAGE_NONE,
            'uid' => $user->uid
          ));

          $wrapper = entity_metadata_wrapper('entityform', $entityform);
          // And set the fields
          if ($wrapper) {
            if (isset($wrapper->field_assessment)) {
              $wrapper->field_assessment = $value['aid'];
            }
            if (isset($wrapper->field_submission_method)) {
              $wrapper->field_submission_method = 'CSV';
            }

            foreach ($value as $source => $source_value) {
              if (in_array($source, $header_fields)) {
                $wrapper->{$source} = $source_value;
              }
            }

            $wrapper->save();
          }

          $messages[] = t('The line !line has been imported successfully.', array('!line' => $counter));
        }
        catch(Exception $e){
          watchdog('test_assessment', $e->getMessage());
          $messages[] = t('Line !line was not imported.', array('!line' => $counter));
        }
      }
      else {
        $messages[] = t('The line !line doesn\'t any values.', array('!line' => $counter));
      }
    }
    fclose($handle);
  }
  catch (Exception $e) {
    watchdog('test_assessment', $e->getMessage());
    $messages[] = t('The file @file cannot be read', array('@file' => $file->uri));
  }

  if (count($messages)){
    drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }
}

/**
 * Helper function - import questionnaires submissions from paper.
 *
 * @param $fid
 * @param $aid
 * @param $entityform_type
 * @param $entityform_label
 * @param $header_fields
 *
 * @throws Exception
 * @return array
 */
function _test_assessment_process_submissions_import_paper($fid, $aid, $entityform_type, $entityform_label, $header_fields) {
  global $user;
  $messages = array();

  $file = file_load($fid);
  // Gets associative result array
  try {
    ini_set('auto_detect_line_endings', true);
    $handle = fopen($file->uri, 'r');
    $header = fgetcsv($handle);

    // Check required fields
    $incorrect = array();
    if (!in_array('sid', $header)) {
      $incorrect[] = 'sid';
    }
    if (!in_array('fid', $header)) {
      $incorrect[] = 'fid';
    }


    if (!empty($incorrect)) {
      $be = (count($incorrect) > 1) ? 'were' : 'was';
      $messages[] = t("'%field_names' %be not found on the paper. Please check field names and try again. 0 Rows imported.", array('%field_names' => implode(' , ', $incorrect), '%be' => $be));
      return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
    }

    $counter = 0;
    $mapping = _test_assessment_submissions_reply_mapping();

    // Gets existing submissions
    $query = db_select('field_data_field_submission_code', 'code');
    $query->innerJoin('field_data_field_submission_method', 'method', 'code.entity_id = method.entity_id');
    $query->innerJoin('field_data_field_assessment', 'survey', 'code.entity_id = survey.entity_id');
    $query->fields('code', array('entity_id', 'field_submission_code_value'))
      ->condition('survey.field_assessment_target_id', $aid->nid)
      ->condition('method.field_submission_method_value', 'paper')
      ->condition('code.bundle', $entityform_type)
      ->condition('code.entity_type', 'entityform');

    $existing_submissions = $query->execute()->fetchAllKeyed();

    while (($data = fgetcsv($handle)) !== FALSE) {
      $row_break = FALSE;
      $counter++;

      $line = array();
      foreach ($header as $key => $value) {
        $data_value = (isset($data[$key])) ? $data[$key] : '';
        if ($value == 'sid') {
          if ($data_value != $aid->nid) {
            $messages[] = t("The line !line isn't imported - This submission is for the other survey '%data'", array('!line' => $counter, '%data' => $data[$key]));
            $row_break = TRUE;
          }
          else {
            $line[$value] = $data_value;
          }
        }
        elseif ($value == 'fid') {
          if (isset($existing_submissions) && !empty($existing_submissions) && in_array('CSV.' . $data_value, $existing_submissions)) {
            $messages[] = t("The line !line isn't imported - CSV.'%fid' already exists", array('!line' => $counter, '%fid' => $data_value));
            $row_break = TRUE;
          }
          else {
            $line[$value] = $data_value;
          }
        }
        elseif (in_array($value, $header_fields)) {
          $field = field_info_field($value);
          $allowed_values= list_allowed_values($field);

          if (isset($mapping[$data_value])) {
            $data_value = $mapping[$data_value];
          }

          // Check if the data of the field is correct
          if (isset($allowed_values) && $data_value) {
            if (array_key_exists($data_value, $allowed_values)) {
              $line[$value] = $data_value;
            }
            else {
              $messages[] = t("The line !line failed - invalid data '%data' for the field '%value'", array('!line' => $counter, '%data' => $data_value, '%value' => $value));
              $row_break = TRUE;
            }
          }
          else {
            $line[$value] = $data_value;
          }
        }
      }

      if ($row_break) {
        $messages[] = t("The line !line has not been imported.", array('!line' => $counter));
        continue;
      }

      // Creates entity 'entityform'
      if ($value = array_filter($line)) {
        try {
          // Create submission
          $entityform = entity_create('entityform', array(
            'type' => $entityform_type,
            'created' => time(),
            'changed' => time(),
            'language' => LANGUAGE_NONE,
            'uid' => $user->uid
          ));

          $wrapper = entity_metadata_wrapper('entityform', $entityform);
          // And set the fields
          if ($wrapper) {
            if (isset($wrapper->field_assessment)) {
              $wrapper->field_assessment = $value['sid'];
            }
            if (isset($wrapper->field_submission_method)) {
              $wrapper->field_submission_method = 'paper';
            }
            if (isset($wrapper->field_submission_code)) {
              $wrapper->field_submission_code = 'CSV.' . $value['fid'];
            }

            foreach ($value as $source => $source_value) {
              if (in_array($source, $header_fields)) {
                $wrapper->{$source} = $source_value;
              }
            }

            $wrapper->save();
          }

          $messages[] = t('The line !line has been imported successfully.', array('!line' => $counter));
        }
        catch(Exception $e){
          watchdog('test_assessment', $e->getMessage());
          $messages[] = t('Line !line was not imported.', array('!line' => $counter));
        }
      }
      else {
        $messages[] = t('The line !line doesn\'t any values.', array('!line' => $counter));
      }
    }
    fclose($handle);
  }
  catch (Exception $e) {
    watchdog('test_assessment', $e->getMessage());
    $messages[] = t('The file @file cannot be read', array('@file' => $file->uri));
  }

  if (count($messages)){
    drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }
}

/**
 * Static caching helper to set value mapping for paper submissions.
 */
function _test_assessment_submissions_reply_mapping($mapping = NULL) {
  $mapping = &drupal_static(__FUNCTION__);

  if (empty($mapping)) {
    $mapping = array(
      1 => 'Strongly_agree',
      2 => 'Agree',
      3 => 'Somewhat_agree',
      4 => 'Neither_agree_nor_disagree',
      5 => 'Somewhat_disagree',
      6 => 'Disagree',
      7 => 'Strongly_disagree',
    );
  }

  return $mapping;
}

/**
 * Helper function - import questionnaires submissions from PDF.
 *
 * @param $fid
 * @param $aid
 * @param $entityform_type
 * @param $entityform_label
 * @param $header_fields
 *
 * @throws Exception
 * @return array
 */
function _test_assessment_process_submissions_import_pdf($fid, $aid, $entityform_type, $entityform_label, $header_fields) {
  global $user;
  $messages = array();
  $skip_fields = array(
    'form_build_id',
    'form_token',
    'form_id',
    'Submit'
  );

  // Load PDF file
  try {
    $file = file_load($fid);

    $file = drupal_realpath($file->uri);

    module_load_include('inc', 'test_assessment', 'test_assessment.pdf_imports');

    $pdf = new mikehaertl\pdftk\Pdf($file);
    $data = $pdf->getDataFields();

    if (empty($data)) {
      watchdog('test_assessment', t('The file @file cannot be read', array('@file' => $file)));
      $messages[] = t('The file @file has no data.', array('@file' => $file));
    }

    preg_match_all("/FieldValue: ([^\n\r]*)/ims", $data, $field_values);
    preg_match_all("/FieldName: ([^\n\r]*)/ims", $data, $field_names);

    $field_names = $field_names[1];
    $field_values = $field_values[1];

    $row_break = FALSE;

    // Check if header is correct
    $incorrect = array();

    foreach ($field_names as $field_id => $source_name) {
      if (!in_array($source_name, $skip_fields)) {
        $field_names[$field_id] = $source_name = str_replace('[und]', '', $field_names[$field_id]);
        if (!in_array($source_name, $header_fields)) {
          $incorrect[] = $source_name;
        }
      } else {
        unset($field_names[$field_id]);
      }
    }

    if (!empty($incorrect)) {
      $be = (count($incorrect) > 1) ? 'were' : 'was';
      $messages[] = t("'%source_names' %be not found on Questionnaire '%questionnaire_label'. Please check field names and try again. 0 Rows imported.", array('%source_name' => implode(' , ', $incorrect), '%be' => $be, '%questionnaire_label' => $entityform_label));
      return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
    }
    $counter = 0;
    $line = array();
    foreach ($field_names as $key => $value) {
      $counter++;
      $field = field_info_field($value);
      $allowed_values= list_allowed_values($field);
      $data_value = (isset($field_values[$key])) ? $field_values[$key] : '';
      // Check if the data of the field is correct
      if (isset($allowed_values) && $data_value) {
        if (array_key_exists($field_values[$key], $allowed_values)) {
          $line[$value] = $data_value;
        }
        else {
          $messages[] = t("The line !line failed - invalid data '%data' for the field '%value'", array('!line' => $counter, '%data' => $data[$key], '%value' => $value));
          $row_break = TRUE;
        }
      }
      else {
        $line[$value] = $data_value;
      }
    }

    if ($row_break) {
      $messages[] = t("The PDF %name has not been imported.", array('%name' => $file));
    }

    // Creates entity 'entityform'
    if ($value = array_filter($line)) {
      // ID of the assessment
      $value['aid'] = $aid->nid;

      try {
        // Create submission
        $entityform = entity_create('entityform', array(
          'type' => $entityform_type,
          'created' => REQUEST_TIME,
          'changed' => REQUEST_TIME,
          'language' => LANGUAGE_NONE,
          'uid' => $user->uid
        ));

        $wrapper = entity_metadata_wrapper('entityform', $entityform);
        // And set the fields
        if ($wrapper) {
          if (isset($wrapper->field_assessment)) {
            $wrapper->field_assessment = $value['aid'];
          }
          if (isset($wrapper->field_submission_method)) {
            $wrapper->field_submission_method = 'PDF';
          }

          foreach ($value as $source => $source_value) {
            if (in_array($source, $header_fields)) {
              $wrapper->{$source} = $source_value;
            }
          }

          $wrapper->save();
        }

        $messages[] = t('The line !line has been imported successfully.', array('!line' => $counter));
      }
      catch(Exception $e){
        watchdog('test_assessment', $e->getMessage());
        $messages[] = t('Line !line was not imported.', array('!line' => $counter));
      }
    }
    else {
      $messages[] = t('The line !line doesn\'t any values.', array('!line' => $counter));
    }
  }
  catch (Exception $e) {
    watchdog('test_assessment', $e->getMessage());
    $messages[] = t('The file @file cannot be read', array('@file' => $file->uri));
  }

  if (count($messages)){
    drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }
}

/**
 * Helper function - generates submissions tokens for assessments.
 *
 * @return array
 */
function _test_assessment_create_token($count = 0) {
  $tokens = array();
  if ($count > 0) {
    $existing_tokens_query = db_select('field_data_field_submission_token', 'fst')
      ->fields('fst', array('field_submission_token_value'))
      ->execute();
    $existing_email_codes = db_select('test_email', 'e')
      ->fields('e', array('code'))
      ->execute();
    $existing_tokens = array();
    if ($existing_tokens_query) {
      foreach ($existing_tokens_query as $token) {
        $existing_tokens[] = $token->field_submission_token_value;
      }
    }
    if ($existing_email_codes) {
      foreach ($existing_email_codes as $code) {
        $existing_tokens[] = $code;
      }
    }

    while (count($tokens) < $count) {
      $token = _test_assessment_random_string(6);
      if (!in_array($token, $existing_tokens)) {
        if (empty($tokens)) {
          $tokens[] = $token;
        }
        elseif(!in_array($token, $tokens)) {
          $tokens[] = $token;
        }
      }
    }
  }

  return $tokens;
}

/**
 * Helper function - generates random string.
 *
 * @param $length
 *   Length of the generated string.
 *
 * @return string
 */
function _test_assessment_random_string($length = 6) {
  $password = '';
  $alphabet = '346789ABCDEFGHJKLMNPQRTUVWXY';
  $alphabet_length = strlen($alphabet)-1;

  while ($length--) {
    $key = mt_rand(0, $alphabet_length);
    $password .= $alphabet[$key];
  }

  return $password;
}

/**
 * Helper function - email validation
 * @param $email
 * @return int|mixed
 */
function _test_assessment_email_is_valid($email){
  if (function_exists('filter_var')){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  } else {
    $pattern = '^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$';
    return preg_match($pattern, $email);
  }
}

/**
 * Helper function - gets allowed submissions for assessment
 *
 * @param $node
 *   The $node object.
 * @param $field_collection
 *   The name of the field_collection
 * @param $field_name
 *   The name of the required field in collection
 * @param $value
 *
 * @return string
 */
function _test_assessment_get_collection_field($node, $field_collection, $field_name, $value = 'value') {
  $field_values = NULL;

  $collection_field = field_get_items('node', $node, $field_collection);
  if (isset($collection_field) && !empty($collection_field)){
    foreach ($collection_field as $item) {
      $collection_id = $item['value'];
      $collection = entity_load('field_collection_item', array($collection_id));
      if (isset($collection[$collection_id])) {
        $field = field_get_items('field_collection_item', $collection[$collection_id], $field_name);
        if (isset($field) && !empty($field)) {
          foreach ($field as $el) {
            $field_values[] = $el[$value];
          }
        }
      }
    }
  }

  return $field_values;
}

/**
 * Helper function - gets existing submissions for assessment
 *
 * @param $node
 * @param $submission_type
 *
 * @return string
 */
function _test_assessment_get_existing_submissions($node, $submission_type = ''){
  $result = 0;

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'entityform')
    ->fieldCondition('field_assessment', 'target_id', $node->nid, '=');

  if ($submission_type) {
    $query->fieldCondition('field_submission_method', 'value', $submission_type, '=');
    if ($submission_type == 'token') {
      $token = ($field_values = _test_assessment_get_collection_field($node, 'field_submission_tokens', 'field_submission_token')) ? $field_values[0] : '';
      if ($token) {
        $query->fieldCondition('field_submission_code', 'value', $token, '=');
      }
      else {
        return 0;
      }
    }
  }

  $result = $query->count()->execute();

  return $result;
}

/**
 * Helper function - gets existing emails for assessment
 *
 * @param $node
 *
 * @return string
 */
function _test_assessment_get_existing_emails($node){
  $result = 0;

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'test_email')
    ->propertyCondition('aid', $node->nid);

  $result = $query->count()->execute();

  return $result;
}

/**
 * Emails generation and sending.
 *
 * @param $sid
 * @param $record
 *
 * @return mixed
 */
function _test_assessment_emails_sending($sid, $record) {
  $assessment_node = node_load($sid);
  $assessment_wrapper = entity_metadata_wrapper('node', $assessment_node);
  $tokens = _test_assessment_create_token(1);
  if (isset($assessment_node->field_langcode[LANGUAGE_NONE])) {
    $lang_code = $assessment_node->field_langcode[LANGUAGE_NONE][0]['value'];
  } else {
    $lang_code = 'en';
  }
  //Find defaults if assessor did not fill template
  $test_assess_email_body_test9_default = i18n_variable_get('test_assess_email_body_test9', $lang_code);
  $test_assess_email_body_test9_default = (!empty($test_assess_email_body_test9_default['value'])) ? $test_assess_email_body_test9_default : '';

  $test_assess_email_body_test60_default = i18n_variable_get('test_assess_email_body_test60', $lang_code);
  $test_assess_email_body_test60_default = (!empty($test_assess_email_body_test60_default['value'])) ? $test_assess_email_body_test60_default : '';

  if($assessment_wrapper->field_questionnaire->value()->type == 'test9_questions') {
    $default_body_value = $test_assess_email_body_test9_default['value'];
    $default_subject_value = i18n_variable_get('test_assess_email_sub_test9', $lang_code);
  } else {
    $default_body_value = $test_assess_email_body_test60_default['value'];
    $default_subject_value = i18n_variable_get('test_assess_email_sub_test60', $lang_code);
  }

  $body_array = $assessment_wrapper->field_email_body->value();
  $assessment_node_subject = $assessment_wrapper->field_email_subject->value();
  $body = isset($body_array['value']) ? $body_array['value'] : $default_body_value;
  $subject = isset($assessment_node_subject) ? $assessment_node_subject : $default_subject_value;
  $data = array();
  $data['assessment_id'] = $sid;
  $data['code'] = $tokens[0];
  $data['recipient'] = $record['recipient'];
  $data['recipient_lastname'] = (isset($record['recipient_lastname'])) ? $record['recipient_lastname'] : '';

  $params = array(
    'subject' => token_replace($subject, $data),
    'body' => token_replace($body, $data),
  );
  $nodewrapper = entity_metadata_wrapper('test_email', $record['eid']);
  $nodewrapper->subject = $params['subject'];
  $nodewrapper->body = $params['body'];
  $nodewrapper->code = $tokens[0];
  $nodewrapper->sent = time();
  $nodewrapper->save();

  // Generate fullname for recipient
  $recipient_name = $data['recipient'];
  if (isset($data['recipient_lastname']) && !empty($data['recipient_lastname'])) {
    $recipient_name .= ' ' . $data['recipient_lastname'];
  }

  $send_to = array(
    array(
      'email' => $record['email'],
      'name' => $recipient_name,
      'type' => 'to'
    )
  );
  $org_name = _test_assessment_get_organisation_logo($sid);
  if(isset($org_name) && $org_name != FALSE) {
    $org_image = '<img src='.$org_name.' alt="organisation logo" />';
  }
  else {$org_image = '';}
  //send mail via mandrill system
  // check if sending via mandrill has been disabled
  if (variable_get('test_mandrill_send', FALSE)) {
    // check if we should redirect mail to a testing address
    if (variable_get('test_mandrill_dev_redirect', TRUE)) {
      $send_to[0]['email'] = variable_get('test_mandrill_dev_redirect_address', "admin@bigbluedoor.net");
    }
    _test_assessment_mandrill_send_mail($params['subject'], $params['body'], $send_to, $org_image);
  }
  //drupal_mail('test_assessment', 'test_assessment_send_email', $record['email'], language_default(), $params, variable_get('site_mail', ''));
}

/**
 * Convert hexdec color string to rgb(a) string
 *
 * @param  $color
 *    Hex color
 * @param  $opacity
 * @param  $percent
 *
 * @return string
 */
function _test_assessment_hex2rgba($color, $opacity = FALSE, $percent = FALSE) {
  $default = 'rgb(0,0,0)';

  //Return default if no color provided
  if (empty($color)) {
    return $default;
  }

  //Sanitize $color if "#" is provided
  if ($color[0] == '#') {
    $color = substr($color, 1);
  }

  //Check if color has 6 or 3 characters and get values
  if (strlen($color) == 6) {
    $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
  }
  elseif (strlen($color) == 3) {
    $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
  }
  else {
    return $default;
  }

  //Convert hexadec to rgb
  $rgb =  array_map('hexdec', $hex);

  // Calculate brighter/darker color
  if ($percent) {
    for ($i=0; $i<3; $i++) {
      if ($percent > 0) {
        // Lighter
        $rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
      } else {
        // Darker
        $positivePercent = $percent - ($percent*2);
        $rgb[$i] = round($rgb[$i] * $positivePercent);
      }
      // In case rounding up causes us to go to 256
      if ($rgb[$i] > 255) {
        $rgb[$i] = 255;
      }
    }
  }

  //Check if opacity is set(rgba or rgb)
  if ($opacity) {
    if (abs($opacity) > 1) {
      $opacity = 1.0;
    }
    $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
  }
  else {
    $output = 'rgb('.implode(",",$rgb).')';
  }

  //Return rgb(a) color string
  return $output;
}
