/**
 * Created by viper on 25.10.14.
 */
(function($){
  Drupal.behaviors.test_assessment_report = {

    jquery_version: false,

    attach: function (context, settings) {
      // Do validate when user click on save on test60-questions form
      $('#test-assessment-report-comparison-form #edit-comparison').change(function() {
        var params_str = location.search;
        if($(this).val() == 0) {
          //location.href = location.href.replace(location.search, '');
          location.href = replaceQueryParam('comparison', false, params_str);
        }
        else {
          //location.href = '?comparison=' + $(this).val();
          location.href = replaceQueryParam('comparison', $(this).val(), params_str);
        }
      });

      // Implements 'Indicator' selector
      var test_indicator = $('#test-assessment-report-comparison-form #edit-indicators');

      if (test_indicator.length) {
        var test_indicator_value = sessionStorage.getItem('test_indicator');
        // Set up default value of the indicator selector
        if (test_indicator_value !== null) {
          test_indicator.val(test_indicator_value);
          fadeIndicator(test_indicator);
        }

        test_indicator.change(function(){
          fadeIndicator($(this));
        });
      }
    }
  }

  function fadeIndicator(element) {
    var selected_id = element.val();
    sessionStorage.setItem('test_indicator', selected_id);

    $(".report-content .report-wrap").each(function(){
      if (selected_id == 0) {
        $(this).fadeIn(1000);
        sessionStorage.setItem('test_indicator', null);
      }
      else if ($(this).is("#" + selected_id)) {
        $(this).fadeIn(500);
      }
      else {
        $(this).fadeOut(500);
      }
    });
  }

  function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');

    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
  }

})(jQuery);