(function ($) {

  Drupal.behaviors.test_assessment_report_contextual = {
    attach: function (context) {
      $('form.report-print-form', context).once('print-menu', function () {
        var $wrapper = $(this);
        var $region = $wrapper.closest('.report-wrap');
        var $links = $wrapper.find('.print-menu');
        var $trigger = $('<a class="contextual-links-trigger" href="#" />').text(Drupal.t('Configure')).click(
          function () {
            $links.stop(true, true).slideToggle(100);
            $wrapper.toggleClass('contextual-links-active');
            return false;
          }
        );
        // Attach hover behavior to trigger and ul.contextual-links.
        $trigger.add($links).hover(
          function () { $region.addClass('contextual-links-region-active'); },
          function () { $region.removeClass('contextual-links-region-active'); }
        );
        // Hide the contextual links when user clicks a link or rolls out of the .contextual-links-region.
        $region.bind('mouseleave click', Drupal.contextualLinks.mouseleave);
        $region.hover(
          function() { $trigger.addClass('contextual-links-trigger-active'); },
          function() { $trigger.removeClass('contextual-links-trigger-active'); }
        );
        // Prepend the trigger.
        $wrapper.prepend($trigger);
      });

      $('.table-print-btn', context).click(function() {
        var tableHtml  = '';
        var intro = '';
        var content = '';

        $(this).attr('class').split(' ').map(function(cls) {
          prefixClass = cls.substr(0, 4);

          if (prefixClass === 'gid-') {
            var gid = cls.substring(4);
            var groupElement = $('#' + gid);

            if (groupElement !== 'undefined' && groupElement !== 'null') {
              intro = groupElement.find('div.report-intro');
              if (intro !== 'undefined' && intro.length > 0) {
                tableHtml += intro.clone().wrap('<div>').parent().html();
              }
              content = groupElement.find('table.contextmenu-table');
              if (content !== 'undefined' && content.length > 0) {
                tableHtml += content.clone().wrap('<div>').parent().html();
              }
            }
          }
        });

        if (tableHtml !== 'undefined' && tableHtml !== 'null') {
          var strFrameName = ("printer-" + (new Date()).getTime());
          var jFrame = $( "<iframe name='" + strFrameName + "'>" );
          // Hide the frame and attach to the body.
          jFrame.css("width", "1px").css("height", "1px").css("position", "absolute").css("left", "-9999px").appendTo($("body:first"));
          // Get a FRAMES reference to the new frame.
          var objFrame = window.frames[ strFrameName ];
          // Get a reference to the DOM in the new frame.
          var objDoc = objFrame.document;
          var jStyleDiv = $("<div>").append($("style").clone());
          // Write the HTML for the document. In this, we will
          // write out the HTML of the current element.
          objDoc.open();
          objDoc.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" );
          objDoc.write("<html>");
          objDoc.write("<body>");
          objDoc.write("<head>");
          objDoc.write("<title>");
          objDoc.write(document.title);
          objDoc.write("</title>");
          objDoc.write(jStyleDiv.html());
          objDoc.write("</head>");
          objDoc.write(tableHtml);
          objDoc.write("</body>");
          objDoc.write("</html>");
          objDoc.close();
          // Print the document.
          objFrame.focus();
          objFrame.print();

          setTimeout(function() {
              jFrame.remove();
            }, (60 * 1000)
          );
        }
      });
    }
  };

  /**
   * Disables outline for the region contextual links are associated with.
   */
  Drupal.contextualLinks.mouseleave = function () {
    $(this)
      .find('.contextual-links-active').removeClass('contextual-links-active')
      .find('.print-menu').hide();
  };

})(jQuery);
