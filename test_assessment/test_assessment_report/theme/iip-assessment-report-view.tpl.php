<?php

/**
 * @file
 * This template handles the layout of the views Survey report.
 *
 * Variables available:
 * - $header: Contains:
 * - $header['links']: The switcher by different displays. May be optional.
 * - $header['title']: May be optional.
 * - $header['intro']: Intro text. May be optional.
 * - $header['date_range']: Date range of the submissions. May be optional.
 * - $header['number_submissions']: The number of the submissions.
 * - $header['filters']: The filter form. May be optional.
 * - $view: The survey reports content.
 */
?>

<?php if (isset($header['links']) && !empty($header['links'])): ?>
  <div class="switcher-display"><?php print $header['links']; ?></div>
<?php endif; ?>

<?php if (isset($sidebar) && !empty($sidebar)): ?>
  <aside class="region-content-sidebar"><?php print $sidebar; ?></aside>
<?php endif; ?>

<div class="region-content-wrapper">
  <?php if (isset($header['date_range']) && !empty($header['date_range'])): ?>
    <div class="date-range">
      <span>Date range:</span> <?php print implode(' to ', $header['date_range']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($header['number_submissions']) && !empty($header['number_submissions'])): ?>
    <div class="number-subm">
      <span>Submissions:</span> <?php print $header['number_submissions']; ?>
    </div>
  <?php endif; ?>
  <br/>
  <?php if (isset($header['filters']) && !empty($header['filters'])): ?>
    <div class="report-filter"><?php print $header['filters']; ?></div>
  <?php endif; ?>

  <div class="report-content">
    <?php print $view; ?>
  </div>

  <?php if (isset($footer['filters']) && !empty($footer['filters'])): ?>
    <div class="footer-section">
      <div class="report-filter"><?php print $footer['filters']; ?></div>
    </div>
  <?php endif; ?>
</div>
