<?php

/**
 * @file
 * This template handles the layout of the views Survey report - Overview display.
 *
 * Variables available:
 * - $content: Contains:
 * - $content['title'].
 * - $content['intro']: Intro text. May be optional.
 * - $content['table']: Views alignment results as table. May be optional.
 * - $content['comparison_form']: Form for comparing results. May be optional.
 * - $content['graph']: Views alignment results as graph. May be optional.
 * - $item_id
 */
?>

<div<?php if(isset($item_id) && !empty($item_id)) print ' id="' . $item_id . '"'; ?> class="report-wrap">
  <?php if (isset($content['intro']) || isset($content['title']) || isset($content['responses'])): ?>
    <div class="report-intro">
      <?php if (isset($content['title']) && !empty($content['title'])): ?>
        <h2 style="font-weight:100;text-align:center;"><?php print $content['title']; ?></h2>
      <?php endif; ?>
      <?php if (isset($content['intro']) && !empty($content['intro'])): ?>
        <div class="intro">
          <?php print $content['intro']; ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['responses']) && !empty($content['responses'])): ?>
        <div class="report-responses" style="text-align:center;">
          <span>Responses</span> <?php print $content['responses']; ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  <?php if (isset($content['group_chart']) && !empty($content['group_chart'])): ?>
    <div class="group-chart">
      <?php print $content['group_chart']; ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['table']) && !empty($content['table'])): ?>
    <?php print $content['table']; ?>
  <?php endif; ?>

  <?php if (isset($content['graph']) && !empty($content['graph'])): ?>
    <div class="overview-report-graph">
      <?php if (isset($content['comparison_form']) && !empty($content['comparison_form'])): ?>
        <div class="overview-report-graph-form"><?php print $content['comparison_form']; ?></div>
      <?php endif; ?>

      <?php if (isset($content['aside_filter_form']) && !empty($content['aside_filter_form'])): ?>
        <div class="report-holder">
          <div class="report-aside-form"><?php print $content['aside_filter_form']; ?></div>
      <?php endif; ?>
          <div class="graph-content"><?php print $content['graph']; ?></div>
      <?php if (isset($content['aside_filter_form']) && !empty($content['aside_filter_form'])): ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>
