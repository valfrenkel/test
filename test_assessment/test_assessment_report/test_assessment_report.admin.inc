<?php

/**
 * Admin system settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 *   Form API render array.
 */
function test_assessment_chart_color_scheme_admin_form($form, $form_state) {

  $colors = variable_get('test_assessment_report_chart_colors', array());
  // Gets all types of the entityform
  $list_allowed_values = array();
  $entityforms = entity_get_info('entityform');
  if (isset($entityforms['bundles']) && !empty($entityforms['bundles'])) {
    foreach ($entityforms['bundles'] as $bundle => $bundle_info) {
      $entityform_type = entityform_type_load($bundle);

      if (isset($entityform_type->data['surveys_flag']) && $entityform_type->data['surveys_flag']) {
        foreach (field_info_instances('entityform', $bundle) as $field_name => $instance) {
          $field_info = field_info_field($field_name);
          if (isset($field_info['module']) && $field_info['module'] == 'list') {
            $allowed_value = list_allowed_values($field_info);

            if (isset($allowed_value) && !empty($allowed_value)) {
              $list_allowed_values = array_merge($list_allowed_values, $allowed_value);
            }
          }
        }
      }
    }
  }

  $form['#tree'] = TRUE;
  $form['#attributes'] = array('enctype'  => "multipart/form-data");
  if ($list_allowed_values) {
    foreach ($list_allowed_values as $key => $value) {
      $form['test_assessment_report'][$key]['answer'] = array(
        '#markup' => $value,
      );
      $form['test_assessment_report'][$key]['color'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($colors[$key]) ? $colors[$key] : '#333333',
      );
    }

    // Adds element for 'No answer'
    $form['test_assessment_report']['No answer']['answer'] = array(
      '#markup' => t('No answer'),
    );
    $form['test_assessment_report']['No answer']['color'] = array(
      '#type' => 'textfield',
      '#default_value' => isset($colors['No answer']) ? $colors['No answer'] : '#CCCCCC',
    );
  }

  // Use the default system settings actions ...
  $form = system_settings_form($form);
  // Custom submit handler
  $form['#submit'] = array(
    'test_assessment_chart_color_scheme_admin_form_submit',
  );

  $form['#theme'] = array('test_assessment_chart_color_scheme_admin_form');
  return $form;
}

/**
 * Submission handler for admin system settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function test_assessment_chart_color_scheme_admin_form_submit($form, &$form_state) {
  $colors = array();
  // Set color for the chart
  if (isset($form_state['values']['test_assessment_report'])) {
    foreach ($form_state['values']['test_assessment_report'] as $key => $value) {
      $colors[$key] = $value['color'];
    }
  }

  variable_set('test_assessment_report_chart_colors', $colors);
  drupal_set_message(t('The color scheme has been saved.'));
}

/**
 * Admin form to import "industry baseline".
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function test_assessment_import_baseline_admin_form($form, $form_state) {
  $form['test_assessment_baseline_data'] = array(
    '#type' => 'managed_file',
    '#title' => t('Baseline surveys data'),
    '#title_display' => 'before',
    '#size' => 22,
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
    ),
    '#required' => TRUE,
  );

  $form['test_assessment_baseline_import'] = array(
    '#type' => 'submit',
    '#value' => t('Import data'),
  );

  return $form;
}

/**
 * Submission handler for admin system settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function test_assessment_import_baseline_admin_form_submit($form, &$form_state) {
  module_load_include('inc', 'test_assessment', 'test_assessment.common');

  global $user;
  $values = $form_state['values'];
  $messages = array();

  // Check that content-type 'survey_baseline_data' exists
  if (!array_key_exists('survey_baseline_data', node_type_get_names())) {
    $messages[] = t('The import is impossible. Please contact administrator.');
    watchdog('test_assessment_report', 'Desired content-type "survey_baseline_data" doesn\'t exist.');
    return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }

  $file = file_load($values['test_assessment_baseline_data']);
  // Gets associative result array
  try {
    ini_set('auto_detect_line_endings', true);
    $handle = fopen($file->uri, 'r');
    $header = fgetcsv($handle);

    $key_industry = array_search('field_sector_industry', $header, TRUE);
    $key_questionnaire = array_search('field_questionnaire', $header, TRUE);
    $field_name = array_search('field_questionnaire', $header, TRUE);
    if ($key_industry !== FALSE && $key_questionnaire !== FALSE && $field_name !== FALSE) {
      unset($header[$key_industry]);
      unset($header[$key_questionnaire]);
      unset($header[$field_name]);

      $collection_info = field_info_instances('field_collection_item', 'field_baseline_answers');
      $counter = 0;
      $failed_emails = 0;
      while (($data = fgetcsv($handle)) !== FALSE) {
        $counter++;
        // Gets ID of the questionanire by its name
        $questionnaire = entityform_get_types($data[$key_questionnaire]);
        if (!isset($questionnaire->id)) {
          $messages[] = t('Line !line have incorrect type of the questionnare', array('!line' => $counter));
          $failed_emails++;
          continue;
        }

        $qid = $questionnaire->id;
        $query = new EntityFieldQuery;
        $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'survey_baseline_data')
          ->fieldCondition('field_sector_industry', 'value', $data[$key_industry], '=')
          ->fieldCondition('field_questionnaire', 'target_id', $qid, '=');

        $result = $query->execute();
        if (isset($result['node']) && !empty($result['node'])) {
          $item = array_shift($result['node']);
          $node = node_load($item->nid);
          $wrapper = entity_metadata_wrapper('node', $node);
        }
        else {
          try {
            // Created new node "survey-baseline-data"
            $node = entity_create('node', array(
              'type' => 'survey_baseline_data',
              'language' => LANGUAGE_NONE,
              'created' => time(),
              'changed' => time(),
              'uid' => $user->uid,
            ));
            $wrapper = entity_metadata_wrapper('node', $node);
            $wrapper->field_sector_industry->set($data[$key_industry]);
            $wrapper->field_questionnaire->set($qid);
            $wrapper->save();
          }
          catch(Exception $e) {
            watchdog('survey_baseline_data', $e->getMessage());
            $messages[] = t('Line !line was not imported.', array('!line' => $counter));
            $failed_emails++;
          }
        }

        // Import each row to field_collection item
        // Create the collection entity and set it's "host".
        if (isset($node->nid)) {
          $field_machine_name = array_search('field_machine_name', $header, TRUE);

          // Get list of the existed fields
          $query_exists_fields = db_select('field_data_field_machine_name', 'mname');
          $query_exists_fields->innerJoin('field_data_field_baseline_answers', 'ba', 'mname.entity_id = ba.field_baseline_answers_value');
          $query_exists_fields->fields('mname', array('entity_id', 'field_machine_name_value'));
          $query_exists_fields->condition('ba.entity_id', $node->nid)
              ->condition('mname.entity_type', 'field_collection_item')
              ->condition('mname.bundle', 'field_baseline_answers')
              ->condition('ba.entity_type', 'node')
              ->condition('ba.bundle', 'survey_baseline_data');

          $exists_fields = $query_exists_fields->execute()->fetchAllKeyed();
          if (isset($exists_fields) && !empty($exists_fields) && $key = array_search($data[$field_machine_name], $exists_fields)) {
            $collection = field_collection_item_load($key);
            $collection_wrap = entity_metadata_wrapper('field_collection_item', $collection);
          }
          else {
            $collection = entity_create('field_collection_item', array('field_name' => 'field_baseline_answers'));
            $collection->setHostEntity('node', $node);
            $collection_wrap = entity_metadata_wrapper('field_collection_item', $collection);
          }

          foreach ($header as $key => $field_name) {
            if (isset($collection_info) && is_array($collection_info) && array_key_exists($field_name, $collection_info)) {
              $collection_wrap->{$field_name}->set($data[$key]);
            }
          }

          // Save the entity.
          $collection_wrap->save();
          $title = l($node->title, 'node/' . $node->nid);
          $messages[$node->nid] = t('Data for "Baseline surveys data" !title has been imported successfully.', array('!title' => $title));
        }
      }
    }
    else {
      $messages[] = t('Source file is incorrect. Please check that it contains required fields "field_sector_industry", "field_questionnaire" and "field_machine_name"');
      return drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
    }

    fclose($handle);
  }
  catch (Exception $e) {
    watchdog('test_assessment_report', $e->getMessage());
    $messages[] = t('The file @file cannot be read', array('@file' => $file->uri));
  }

  if (count($messages)){
    drupal_set_message(theme('item_list', array('items' => $messages,'title' => null,'type' => 'ul','attributes' => array())));
  }
}

/**
 * Renders the test_assessment_report_admin_form
 */
function test_assessment_report_admin_form($form, $form_state) {
  $form['report_intro_texts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Intro texts for report pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Overview page of the report - intro text
  $default_intro_overview = variable_get('test_assessment_report_overview_into', array(
    'value' => '',
    'format' => 'full_html',
  ));
  $form['report_intro_texts']['test_assessment_report_overview_into'] = array(
    '#title' => t('Overview of Survey results - Intro text'),
    '#default_value' => check_markup($default_intro_overview['value'], $default_intro_overview['format']),
    '#format' => $default_intro_overview['format'],
    '#required' => FALSE,
    '#type' => 'text_format'
  );

  // Alignment summary page of the report - intro text
  $default_intro_alignment = variable_get('test_assessment_report_alignment_summary_into', array(
    'value' => '',
    'format' => 'full_html',
  ));
  $form['report_intro_texts']['test_assessment_report_alignment_summary_into'] = array(
    '#title' => t('Alignment summary - Intro text'),
    '#default_value' => check_markup($default_intro_alignment['value'], $default_intro_alignment['format']),
    '#format' => $default_intro_alignment['format'],
    '#required' => FALSE,
    '#type' => 'text_format'
  );

  // Intro text for groups of the TEST40 survey form
  $fields_groups = field_group_info_groups('entityform', 'test60_questions');
  if (isset($fields_groups['form']) && !empty($fields_groups['form'])) {
    $form['test_assessment_report_test60_groups'] = array(
      '#type' => 'fieldset',
      '#title' => t('TEST40 Questions - Groups intro texts'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    // Sort groups by weight
    $weights = array();
    foreach ($fields_groups['form'] as $fgroup_name => $fgroup) {
      $weights[$fgroup_name] = $fgroup->weight;
    }
    array_multisort ($weights, SORT_ASC, SORT_NUMERIC, $fields_groups['form']);

    foreach ($fields_groups['form'] as $group_name => $group) {
      if (isset($group->format_type) && $group->group_name != 'group_test60_page') {
        $default_intro_group = variable_get('test_assessment_report_into_' . $group_name, array(
          'value' => '',
          'format' => 'full_html',
        ));
        $form['test_assessment_report_test60_groups']['test_assessment_report_into_' . $group_name] = array(
          '#title' => t('@label - Intro text', array('@label' => $group->label)),
          '#default_value' => check_markup($default_intro_group['value'], $default_intro_group['format']),
          '#format' => $default_intro_group['format'],
          '#required' => FALSE,
          '#type' => 'text_format',
        );
      }
    }
  }

  return system_settings_form($form);
}
