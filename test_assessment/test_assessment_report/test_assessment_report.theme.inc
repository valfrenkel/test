<?php

/**
 * Returns HTML for the administration form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 * @return string
 *   HTML Markup.
 *
 * @ingroup themeable
 */
function theme_test_assessment_chart_color_scheme_admin_form($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form['test_assessment_report']) as $key) {
    $rows[] = array(
      'data' => array(
        drupal_render($form['test_assessment_report'][$key]['answer']),
        drupal_render($form['test_assessment_report'][$key]['color']),
      )
    );
  }

  $header = array(t('Answer'), t('Color'));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'chart-color-scheme')));
  $output .= drupal_render_children($form);

  return $output;
}