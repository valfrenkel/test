<?php
/**
 * @file
 * Block functions for TEST Assestment feature.
 *
 */

/**
 * Implements hook_block_info().
 *
 * @see hook_block_info()
 */
function test_assessment_block_info() {
  $blocks = array();
  
  //add block for progress indicator assessment states
  $blocks['assessment_progress_indicator'] = array(
    'info' => t('Shows progress indicator for Survey feature'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function test_assessment_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'assessment_progress_indicator':
      $block['subject'] = '';
      $block['content'] = assessment_progress_indicator();
      break;
  }
  return $block;
}

/**
 * Render Assessment Progress Indicator 
 */

function assessment_progress_indicator() {
  $survey_state = 'start';
  if (arg(3)) {
    if (($node = node_load(arg(3))) && ($node->type == 'assessment')) {
      $survey_state_field = field_get_items('node', $node, 'field_survey_state');
      $survey_state = (isset($survey_state_field[0]['value']) && !empty($survey_state_field[0]['value'])) ? $survey_state_field[0]['value'] : 'start';
    }
  }
  $survey_state_array = array(
    'start' => 'Start', 
    'draft' => 'Draft', 
    'ready' => 'Ready', 
    'active' => 'Active',
    'closed' => 'Closed',  
    'archived' => 'Archived',
    );
  $output = "<ul class='assessments_progress_indicator'>";
  foreach ($survey_state_array as $key => $state) {
    $class = ($key == $survey_state) ? 'current ' . $key  : $key;
    $output .= '<li class="'.$class.'">' . t($state). '</li>';
  }

  $output .= "</ul>";
  return $output;
}
