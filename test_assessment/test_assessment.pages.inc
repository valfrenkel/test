<?php

/**
 * Menu callback - builds modal page - Add optional questions.
 */
function test_assessment_customise_questionnaire_form_callback($node = NULL, $js = FALSE) {
  if (isset($node)) {
    // Set form state
    $form_state = array(
      'node' => $node,
    );
    if ($js) {
      ctools_include('modal');
      ctools_include('ajax');
      ctools_add_js('ajax-responder');

      $form_state += array(
        'ajax' => TRUE,
        're_render' => TRUE,
      );
      // Render upload form form as modal form
      $output = ctools_modal_form_wrapper('test_assessment_customise_questionnaire_form', $form_state);
      if (!empty($form_state['executed'])) {
        // Close modal form after submittion
        $output = array();
        $output[] = ctools_ajax_command_reload();
        $output[] = ctools_modal_command_dismiss();
      }

      print ajax_render($output);
      drupal_exit();
    }
    else {
      return drupal_build_form('test_assessment_customise_questionnaire_form', $form_state);
    }
  }
}

/**
 * Form callback - builds form for customising questionnaire.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 */
function test_assessment_customise_questionnaire_form($form, &$form_state){
  $form['#parents'] = array();

  // variables are needed to build field forms.
  $entity_type = 'node';
  $bundle_name = 'assessment';
  $entity = $form_state['node'];
  $langcode = (isset($entity->language) && !empty($entity->language)) ? $entity->language : LANGUAGE_NONE;
  // specify field to add
  $field_name = 'field_optional_entityform_fields';

  $items = field_get_items($entity_type, $entity, $field_name);
  $wrapper = entity_metadata_wrapper('node', $entity);
  $questionnaire = $wrapper->field_questionnaire->value()->type;
  $options = test_assessment_get_optional_questions($questionnaire);
  $optional_questions = $wrapper->field_optional_entityform_fields->value();

  // We need the field and field instance information to pass along to field_default_form().
  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle_name);

  $field_form = field_default_form($entity_type, $entity, $field, $instance, $langcode, $items, $form, $form_state);
  $field_form['field_optional_entityform_fields'][$langcode]['#options'] = $options;
  $field_form['field_optional_entityform_fields'][$langcode]['#default_value'] = $optional_questions;

  $form += $field_form;
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Yes'));
  $form['actions']['cancel'] = array(
    '#markup' => l(t('No'), '#', array(
      'attributes' => array(
        'class' => array('ctools-close-modal')
      ),
      'external' => TRUE,
    )),
  );
  return $form;
}

/**
 * Form submission handler for test_assessment_customise_questionnaire_form().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see test_assessment_customise_questionnaire_form()
 */
function test_assessment_customise_questionnaire_form_submit(&$form, &$form_state){
  //we need to save optional questions to node
  $values = $form_state['values'];
  $node = $form_state['node'];
  $langcode = (isset($node->language) && !empty($node->language)) ? $node->language : LANGUAGE_NONE;

  if (isset($node->nid)) {
    $nodewrapper = entity_metadata_wrapper('node', $node);
    $nodewrapper->field_optional_entityform_fields->set($values['field_optional_entityform_fields'][$langcode]);
    $nodewrapper->save();
  }
}

/**
 * Menu callback - builds modal page.
 */
function test_assessment_ready_start_form_callback($node = NULL, $js = FALSE) {
  if ($_POST['form_id'] == 'test_assessment_draft_form') {
    module_load_include('inc', 'test_assessment', 'test_assessment.admin');
    // build form state
    $state = array();
    // unset unnessessary elements
    unset($_POST['_triggering_element_name']);
    unset($_POST['_triggering_element_value']);
    $state['values']= $_POST;
    $state['values']['nid'] = $node->nid;
    $state['values']['op'] = 'Save Changes';
    $state['node'] = $node;
    $state['rebuild'] = FALSE;
    $state['build_info']['args'][0] = $node;
    $state['programmed'] = FALSE;
    // triger save draft form
    drupal_form_submit('test_assessment_draft_form', $state ,$node);
  }
  if (isset($node)) {
    // Set form state
    $form_state = array(
      'node' => $node,
      'rebuild' => FALSE,
    );

    if ($js) {
      ctools_include('modal');
      ctools_include('ajax');

      $form_state += array(
        'ajax' => TRUE,
        're_render' => TRUE,
        'no_redirect' => TRUE,
      );

      // Render upload form form as modal form
      $output = ctools_modal_form_wrapper('test_assessment_ready_start_form', $form_state);

      if (!empty($form_state['executed'])) {
        // Close modal form after submittion
        $output = array();
        $output[] = ctools_modal_command_dismiss();
      }

      print ajax_render($output);
      drupal_exit();
    }
    else {
      return drupal_build_form('test_assessment_ready_start_form', $form_state);
    }
  }
}
/**
 * Form callback - builds form for moving to ready state.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 */
function test_assessment_ready_start_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['intro_text'] = array(
    '#markup' => t('You are now ready to start your survey. Before doing so, please check...'),
  );

  $default_ready_options = variable_get('ready_option', array('label' => ''));
  foreach ($default_ready_options as $ready_option) {
    if(!empty($ready_option['label'])) {
      $ready_option_id = preg_replace(array('/ /', '/\W/'), array('_', ''), strtolower($ready_option['label']));
      $form[$ready_option_id] = array(
        '#type' => 'checkbox',
        '#title' => $ready_option['label'],
        '#required' => $ready_option['required'],
        '#default_value' => $ready_option['checked'],
      );
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Yes'));
  $form['actions']['cancel'] = array(
    '#markup' => l(t('No'), '#', array(
      'attributes' => array(
        'class' => array('ctools-close-modal')
      ),
      'external' => TRUE,
    )),
  );

  return $form;
}

/**
 * Form submission handler for test_assessment_ready_start_form().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see test_assessment_ready_start_form()
 */
function test_assessment_ready_start_form_submit(&$form, &$form_state) {
  $node = $form_state['node'];
  if (isset($node->nid)) {
    // Change survey status of the assessment to 'ready'
    $nodewrapper = entity_metadata_wrapper('node', node_load($node->nid));
    $nodewrapper->field_survey_state = 'ready';
    $nodewrapper->save();

    drupal_set_message(t('The survey is ready.'));
    // Open modal form and redirect to Ready form
    $path = TEST_ASSESSMENTS_MANAGE_PATH . '/' . $node->nid . '/ready';
    $commands[] = ctools_modal_command_dismiss();
    $commands[] = ctools_ajax_command_redirect($path);
    print ajax_render($commands);
    drupal_exit();
  }
}

/**
 * Menu callback - builds modal page - Edit Email Template.
 */
function test_assessment_edit_email_form_callback($node = NULL, $js = FALSE) {
  if (isset($node)) {
    // Set form state
    $form_state = array(
      'node' => $node,
    );
    if ($js) {
      ctools_include('modal');
      ctools_include('ajax');
      ctools_add_js('ajax-responder');

      $form_state += array(
        'ajax' => TRUE,
        're_render' => TRUE,
      );
      // Render upload form form as modal form
      $output = ctools_modal_form_wrapper('test_assessment_edit_email_form', $form_state);
      if (!empty($form_state['executed'])) {
        // Close modal form after submittion
        $output = array();
        $output[] = ctools_ajax_command_reload();
        $output[] = ctools_modal_command_dismiss();
      }

      print ajax_render($output);
      drupal_exit();
    }
    else {
      return drupal_build_form('test_assessment_edit_email_form', $form_state);
    }
  }
}

/**
 * Form callback - builds form for edit template.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 */
function test_assessment_edit_email_form($form, &$form_state){
  $form['#parents'] = array();
  // variables are needed to build field forms.
  $entity_type = 'node';
  $bundle_name = 'assessment';
  $entity = $form_state['node'];
  if (isset($entity->field_langcode[LANGUAGE_NONE])) {
    $lang_code = $entity->field_langcode[LANGUAGE_NONE][0]['value'];
  } else {
    $lang_code = 'en';
  }
  $wrapper = entity_metadata_wrapper('node', $entity);
  $questionnaire = $wrapper->field_questionnaire->value()->type;
  $survey_state = $wrapper->field_survey_state->value();
  $field_sufix_array = explode('_', $questionnaire);
  $langcode = (isset($entity->language) && !empty($entity->language)) ? $entity->language : LANGUAGE_NONE;
  // specify field to add
  $fields_array = array('field_email_subject', 'field_email_body');

  foreach ($fields_array as $field_name) {
    $items = field_get_items($entity_type, $entity, $field_name);
    // We need the field and field instance information to pass along to field_default_form().
    $field = field_info_field($field_name);
    $instance = field_info_instance($entity_type, $field_name, $bundle_name);
    $field_form = field_default_form($entity_type, $entity, $field, $instance, $langcode, $items, $form, $form_state);

    if ($field_name == "field_email_subject") {
      if(empty($field_form[$field_name][$langcode][0]['value']['#default_value'])) {
        $field_form[$field_name][$langcode][0]['value']['#default_value'] = i18n_variable_get('test_assess_email_sub_'.$field_sufix_array[0], $lang_code);
      }

      if (isset($survey_state) && in_array($survey_state, array('ready', 'active', 'closed'))) {
        $field_form[$field_name][$langcode][0]['value']['#type'] = 'item';
        $field_form[$field_name][$langcode][0]['value']['#markup'] = '<div class="field-content" style="clear: both;">' . $field_form[$field_name][$langcode][0]['value']['#default_value'] . '</div>';
      }
    }

    if ($field_name == "field_email_body") {
      if(empty($field_form[$field_name][$langcode][0]['#default_value'])) {
        $default_body_text = i18n_variable_get('test_assess_email_body_'.$field_sufix_array[0], $lang_code);
        $field_form[$field_name][$langcode][0]['#default_value'] = $default_body_text['value'];
      }

      if (isset($survey_state) && in_array($survey_state, array('ready', 'active', 'closed'))) {
        $field_form[$field_name][$langcode][0]['#type'] = 'item';
        $field_form[$field_name][$langcode][0]['#markup'] = '<div class="field-content" style="clear: both;">' . $field_form[$field_name][$langcode][0]['#default_value'] . '</div>';
      }
    }

    $form += $field_form;
  }

  $form['actions'] = array('#type' => 'actions', '#weight' => 101);
  $cancel_btn = t('Close');
  if (isset($survey_state) && $survey_state == 'draft') {
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));
    $cancel_btn = t('No');
  }

  $form['actions']['cancel'] = array(
    '#markup' => l($cancel_btn, '#', array(
      'attributes' => array(
        'class' => array('ctools-close-modal')
      ),
      'external' => TRUE,
    )),
  );
  return $form;
}

/**
 * Form submission handler for test_assessment_edit_email_form().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see test_assessment_edit_email_form()
 */
function test_assessment_edit_email_form_submit(&$form, &$form_state){
  $values = $form_state['values'];
  $node = $form_state['node'];
  $langcode = (isset($node->language) && !empty($node->language)) ? $node->language : LANGUAGE_NONE;
  if (isset($node->nid)) {
    $nodewrapper = entity_metadata_wrapper('node', $node);
    $nodewrapper->field_email_subject->set($values['field_email_subject'][$langcode][0]['value']);
    $nodewrapper->field_email_body->set(array(
      'value' => $values['field_email_body'][$langcode][0]['value'],
      'format' => $values['field_email_body'][$langcode][0]['format']));
    $nodewrapper->save();

  }
}

/**
 * Menu callback - builds modal page - Edit Reminder Emails.
 */
function test_assessment_edit_reminder_emails_form_callback($node = NULL, $js = FALSE) {
  if (isset($node)) {
    // Set form state
    $form_state = array(
      'node' => $node,
    );
    if ($js) {
      ctools_include('modal');
      ctools_include('ajax');
      ctools_add_js('ajax-responder');

      $form_state += array(
        'ajax' => TRUE,
        're_render' => TRUE,
      );
      // Render upload form form as modal form
      $output = ctools_modal_form_wrapper('test_assessment_edit_reminder_emails_form', $form_state);
      if (!empty($form_state['executed'])) {
        // Close modal form after submittion
        $output = array();
        $output[] = ctools_ajax_command_reload();
        $output[] = ctools_modal_command_dismiss();
      }

      print ajax_render($output);
      drupal_exit();
    }
    else {
      return drupal_build_form('test_assessment_edit_reminder_emails_form', $form_state);
    }
  }
}

/**
 * Form callback - builds form for edit reminder emails.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 */
function test_assessment_edit_reminder_emails_form($form, &$form_state){
  $form['#parents'] = array();
  // variables are needed to build field forms.
  $entity_type = 'node';
  $entity = $form_state['node'];
  $langcode = (isset($entity->language) && !empty($entity->language)) ? $entity->language : LANGUAGE_NONE;
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  $survey_state = $wrapper->field_survey_state->value();
  
  field_attach_form($entity_type, $entity, $form, $form_state, $langcode);
  $fields = array('date', 'sub', 'body');
  foreach (field_info_instances($entity_type, ASSESSMENT_NODE_TYPE) as $field_name => $instance) {
    if (substr($field_name, 0, 20) != 'field_assess_remind_') {
      $form[$field_name]['#access'] = FALSE;
    }

    // Updated form for closed state
    if ($survey_state == 'closed') {
      if (substr($field_name, 0, 20) == 'field_assess_remind_' && in_array(substr($field_name, 22), $fields)) {
        $form[$field_name . '_markup'] = $form[$field_name];

        $title = $form[$field_name][LANGUAGE_NONE]['#title'];
        $value = '';
        if (isset($form[$field_name][LANGUAGE_NONE][0]['#default_value']) && !is_array($form[$field_name][LANGUAGE_NONE][0]['#default_value'])) {
          $value = $form[$field_name][LANGUAGE_NONE][0]['#default_value'];
        } elseif (isset($form[$field_name][LANGUAGE_NONE][0]['#default_value']['value']) && !is_array($form[$field_name][LANGUAGE_NONE][0]['#default_value']['value'])) {
          $value = $form[$field_name][LANGUAGE_NONE][0]['#default_value']['value'];
        } elseif (isset($form[$field_name][LANGUAGE_NONE][0]['value']['#default_value']) && !is_array($form[$field_name][LANGUAGE_NONE][0]['value']['#default_value'])) {
          $value = $form[$field_name][LANGUAGE_NONE][0]['value']['#default_value'];
        }

        if (substr($field_name, 22) == 'date' && $value) {
          $value = date('Y-m-d', strtotime($value));
        }

        unset($form[$field_name . '_markup'][LANGUAGE_NONE]);
        $form[$field_name . '_markup'][LANGUAGE_NONE]['#markup'] = '<div class="field-content" style="clear: both;">';
        $form[$field_name . '_markup'][LANGUAGE_NONE]['#markup'] .= '<b>'. $title . '</b>';
        if ($value && !is_array($value)) {
          $form[$field_name . '_markup'][LANGUAGE_NONE]['#markup'] .= ' ' . $value;
        }
        $form[$field_name . '_markup'][LANGUAGE_NONE]['#markup'] .= '</div><br>';

        // Adds the field to group
        if (isset($form['#group_children'][$field_name])) {
          $group_name = $form['#group_children'][$field_name];
          $form['#group_children'][$field_name . '_markup'] = $group_name;
        }
      }

      $form[$field_name]['#access'] = FALSE;
    }
  }

  // Hide unnecessary section
  if (isset($form['#metatags'])) {
    unset($form['#metatags']);
  }
  if (isset($form['redirect'])) {
    $form['redirect']['#type'] = 'hidden';
  }

  if ($survey_state == 'closed') {
    $form['actions'] = array('#type' => 'actions', '#weight' => 100);
    $form['actions']['close'] = array(
    '#type' => 'item',
    '#markup' => l(t('Close'), '#', array(
      'attributes' => array(
        'class' => array('ctools-close-modal')
      ),
      'external' => TRUE,
    )),
  );
  } else {

    $form['actions'] = array('#type' => 'actions', '#weight' => 101);
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));
    $form['actions']['cancel'] = array(
      '#markup' => l(t('No'), '#', array(
        'attributes' => array(
          'class' => array('ctools-close-modal')
        ),
        'external' => TRUE,
      )),
    );
  }

  return $form;
}

/**
 * Form submission handler for test_assessment_edit_reminder_emails_form().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see test_assessment_edit_reminder_emails_form()
 */
function test_assessment_edit_reminder_emails_form_submit(&$form, &$form_state){
  $values = $form_state['values'];
  $node = $form_state['node'];
  $langcode = (isset($node->language) && !empty($node->language)) ? $node->language : LANGUAGE_NONE;
  if (isset($node->nid)) {
    $nodewrapper = entity_metadata_wrapper('node', $node);

    for ($i = 1; $i <= 3; $i++) {
      $field_assess_remind_sub = 'field_assess_remind_'.$i.'_sub';
      $field_assess_remind_body = 'field_assess_remind_'.$i.'_body';
      $field_assess_remind_date = 'field_assess_remind_'.$i.'_date';
      $field_assess_remind_send = 'field_assess_remind_'.$i.'_dont_send';

      $nodewrapper->{$field_assess_remind_sub}->set($values[$field_assess_remind_sub][$langcode][0]['value']);
      $nodewrapper->{$field_assess_remind_body}->set(array(
        'value' => $values[$field_assess_remind_body][$langcode][0]['value'],
        'format' => $values[$field_assess_remind_body][$langcode][0]['format']
      ));

      $send_date = strtotime($values[$field_assess_remind_date][$langcode][0]['value']);
      $nodewrapper->{$field_assess_remind_date}->set($send_date);
      $nodewrapper->{$field_assess_remind_send}->set($values[$field_assess_remind_send][$langcode][0]['value']);
    }

    $nodewrapper->save();
  }
}


function test_assessment_import_data_file_generate($node){

  $wrapper = entity_metadata_wrapper('node', $node);
  $entity_form = $wrapper->field_questionnaire->value();

  $entity_fields = field_info_instances('entityform', $entity_form->type);
  $active_fields = test_assessment_get_assessment_active_questions($entity_fields, $wrapper->field_optional_entityform_fields->value());

  $data = array();
  $counter = 0;
  foreach ($active_fields as $key => $field){
    $data['MACHINE NAME'][$counter] = $key;
    $counter++;
  }

  $file_name = 'example-submissions.csv';
  if (count($data)) {
    // Serve file download.
    drupal_add_http_header('Pragma', 'public');
    drupal_add_http_header('Expires', '0');
    drupal_add_http_header('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
    drupal_add_http_header('Content-Type', 'text/csv');
    drupal_add_http_header('Content-Disposition', 'attachment; filename=' . $file_name . ';');
    drupal_add_http_header('Content-Transfer-Encoding', 'binary');
    $output = fopen('php://output', 'w');
    foreach ($data as $label => $row){
      fputcsv($output, $row);
    }
    drupal_exit();
  }
}

/**
 * Menu callback - builds modal page - Stop survey.
 */
function test_assessment_survey_stop_form_callback($node = NULL, $js = FALSE) {
  if (isset($node)) {
    // Set form state
    $form_state = array(
      'node' => $node,
    );
    if ($js) {
      ctools_include('modal');
      ctools_include('ajax');
      ctools_add_js('ajax-responder');

      $form_state += array(
        'ajax' => TRUE,
        're_render' => TRUE,
      );
      // Render upload form form as modal form
      $output = ctools_modal_form_wrapper('test_assessment_survey_stop_form', $form_state);
      if (!empty($form_state['executed'])) {
        // Close modal form after submittion
        $output = array();
        $output[] = ctools_ajax_command_reload();
        $output[] = ctools_modal_command_dismiss();
      }

      print ajax_render($output);
      drupal_exit();
    }
    else {
      return drupal_build_form('test_assessment_survey_stop_form', $form_state);
    }
  }
}

/**
 * Form callback - builds form for realisation of the functionality 'Stop survey'.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 */
function test_assessment_survey_stop_form($form, &$form_state){
  $form['#parents'] = array();

  // variables are needed to build field forms.
  $entity_type = 'node';
  $bundle_name = 'assessment';
  $entity = $form_state['node'];
  $langcode = (isset($entity->language) && !empty($entity->language)) ? $entity->language : LANGUAGE_NONE;

  $form['survey_stop_message'] = array(
    '#markup' => t('You have chosen to stop the survey and all related activity. This action cannot be undone. Are you sure you wish to proceed?'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Yes'));
  $form['actions']['cancel'] = array(
    '#markup' => l(t('No'), '#', array(
      'attributes' => array(
        'class' => array('ctools-close-modal', 'ctools-modal-test-modal')
      ),
      'external' => TRUE,
    )),
  );
  return $form;
}

/**
 * Form submission handler for test_assessment_survey_stop_form().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see test_assessment_survey_stop_form()
 */
function test_assessment_survey_stop_form_submit(&$form, &$form_state){
  $node = $form_state['node'];

  if (isset($node->nid)) {
    $nodewrapper = entity_metadata_wrapper('node', $node);

    // Set end date to current date
    $nodewrapper->field_end_date = time();
    // Forbids sending activate and reminder emails
    for ($reminder = 1; $reminder <= 3; $reminder++) {
      $nodewrapper->{'field_assess_remind_' . $reminder . '_dont_send'} = 1;
    }
    $nodewrapper->field_survey_state = 'closed';

    $nodewrapper->save();

    // Get test_emails for current survey
    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', 'test_email')
      ->propertyCondition('aid', $node->nid);

    $emails = $query->execute();
    if (isset($emails['test_email']) && !empty($emails['test_email'])) {
      foreach ($emails['test_email'] as $email_id => $email) {
        $email_wrap = entity_metadata_wrapper('test_email', $email_id);
        $email_wrap->sent = time();
        for ($reminder = 1; $reminder <= 3; $reminder++) {
          $email_wrap->{'reminder_' . $reminder . '_sent'} = time();
        }
        $email_wrap->save();
      }
    }
  }
}