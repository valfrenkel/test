<?php

function test_assessment_submission_token_start($token) {
  if ($access = test_assessment_access_submission($token)) {
    // Fetch the assessment id associated with the token
    $query = db_select("field_data_field_submission_tokens", "submission_tokens")
      ->fields("submission_tokens", array("entity_id"))
      ->condition("token.field_submission_token_value", $token);
    $query->leftJoin("field_data_field_submission_token", "token", "token.entity_id = submission_tokens.field_submission_tokens_value AND token.entity_type = :fci AND token.bundle = :fst", array(":fci" => 'field_collection_item', ":fst" => 'field_submission_tokens'));

    test_assessment_active_token_and_type($token, 'token');

    return test_assesment_submission_start ($query);
  }
  else {
    // Assessments blocked token notification
    $block_token_message = variable_get('assessments_block_token_notification', array(
      'value' => '<p>' . t('The survey token that you have used is no longer working. If you believe this to be an error please contact Investors in People.') . '</p>',
      'format' => 'full_html',
    ));

    return check_markup($block_token_message['value'], $block_token_message['format']);
  }
}

/**
 * Generate example interview form for the survey
 */
function test_assessment_interview_download($survey) {
  $curr_uri = request_uri();
  $ex_pfg = FALSE;
  // Check if it's PDF downloading
  if (isset($curr_uri)) {
    $curr_arg = array_filter(explode('/', $curr_uri));
    if (isset($curr_arg) && !empty($curr_arg)) {
      $first_el = array_shift($curr_arg);
      if ($first_el == 'printpdf') {
        $ex_pfg = TRUE;
      }
    }
  }

  if ($ex_pfg) {
    test_assessment_active_assessment($survey);
    $questionaire_id = field_get_items('node', $survey, 'field_questionnaire');
    $questionaire_type = db_select("entityform_type", "entityform_type")
      ->fields("entityform_type", array("type"))
      ->condition("entityform_type.id", $questionaire_id[0]['target_id'])
      ->execute()->fetchField();
    $questionaire = entityform_empty_load($questionaire_type, 'submit');

    // Let entityform render the form
    module_load_include('inc', 'entityform', 'entityform.admin');
    $entity_form = entityform_form_wrapper($questionaire, 'submit');

    return $entity_form;
  }
  else {
    return MENU_NOT_FOUND;
  }
}

/**
 * Check access for submissions
 *
 * @param $token
 *   The submission token.
 *
 * @return true/false
 */
function test_assessment_access_submission($token) {
  // Check if token exists
  $query = db_select("field_data_field_block_token", "block_token");
  $query->leftJoin("field_data_field_submission_token", "token", "token.entity_id = block_token.entity_id AND token.entity_type = :fci AND token.bundle = :fst", array(":fci" => 'field_collection_item', ":fst" => 'field_submission_tokens'));
  $query->fields("block_token", array("field_block_token_value"))
    ->fields("token", array("field_submission_token_value"))
    ->condition("token.field_submission_token_value", $token);

  $status = $query->execute()->fetchObject();
  if (isset($status->field_submission_token_value)) {
    if ($status->field_block_token_value) {
      return FALSE;
    }

    return TRUE;
  }
  else {
    return FALSE;
  }
}

function test_assessment_submission_email_start ($token) {
  // Fetch the assessment id associated with the token
  $query = db_select("test_email", "test_email")
    ->fields("test_email", array("aid"))
    ->condition("test_email.code", $token);

  test_assessment_active_token_and_type($token, 'email');

  return test_assesment_submission_start ($query);
}

/**
 * Once we have the query for the entity_id / aid of the assessment node - check for the assessment and load the entityform
 *
 * @param $query
 *
 * @return array
 */
function test_assesment_submission_start($query) {
  $entity_form_output = '';
  if ($aid = $query->execute()->fetchField()) {
    $assessment = node_load($aid);
    // set the active assessment to be used from our form_alter()s.
    test_assessment_active_assessment($assessment);
    $token_and_type = test_assessment_active_token_and_type();
    $error_string = t('Sorry - but this submission code does not appear to be valid');
    // check if field_survey_state == 'active', if move on  if no - return error
    if (!isset($assessment->field_survey_state[LANGUAGE_NONE][0]['value']) || $assessment->field_survey_state[LANGUAGE_NONE][0]['value'] != 'active') {
      watchdog('test_assessment', 'Submission was blocked because Survey %aid inactive', array('%aid' => $aid) , WATCHDOG_INFO);
      return $error_string;
    }
    // check the current date is after field_start_date(start) but before field_start_date(end)
    $start_date = strtotime($assessment->field_start_date[LANGUAGE_NONE][0]['value']);
    $end_date = strtotime($assessment->field_end_date[LANGUAGE_NONE][0]['value']);
    $today = strtotime(date("Y-m-d"));
    if($today < $start_date || $today > $end_date) {
      watchdog('test_assessment', 'Submission was blocked because Survey %aid inactive', array('%aid' => $aid) , WATCHDOG_INFO);
      return $error_string;
    }
    
    // check for group tokens 
    if ($token_and_type['type'] == 'token') {
      $wrapper = entity_metadata_wrapper('node', $assessment);
      foreach($wrapper->field_submission_tokens as $collection) {
        if ($collection->field_submission_token->value() == $token_and_type['token']) {
          $field_submissions_allowed = $collection->field_submissions_allowed->value();
          // we need to find all submissions with this code and compare in with $field_submissions_allowed
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'entityform')
            ->fieldCondition('field_submission_code', 'value', $token_and_type['token'], '=');

          $result = $query->execute();
          $used_submission_value = 0;
          if (isset($result['entityform']) && !empty($result['entityform'])) {
            $used_submission_value = count($result['entityform']);
          }

          if ($used_submission_value >= $field_submissions_allowed){
            watchdog('test_assessment', 'Submission was blocked because Token (survey %aid) had no available submissions', array('%aid' => $aid) , WATCHDOG_INFO);
            return $error_string;
          }

          // check whether token is blocked
          if ($collection->field_block_token->value()) {
            watchdog('test_assessment', 'Submission was blocked because Token (survey %aid) have been blocked ', array('%aid' => $aid) , WATCHDOG_INFO);
            return $error_string;
          }
        }
      }
    }
    // check for single tokens 
    if ($token_and_type['type'] == 'email') {
      // we need to check if this token was used
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'test_email')
        ->propertyCondition('completed', 0, '!=')
        ->propertyCondition('code', $token_and_type['token']);
      $result = $query->execute();

      if (!empty($result)) {
        watchdog('test_assessment', 'Submission was blocked because Email code %code had been used (survey %aid)', array('%aid' => $aid, '%code' => $token_and_type['token']) , WATCHDOG_INFO);
        return $error_string;
      }
    }
    
    $questionaire_id = field_get_items('node', $assessment, 'field_questionnaire');
    $questionaire_type = db_select("entityform_type", "entityform_type")
      ->fields("entityform_type", array("type"))
      ->condition("entityform_type.id", $questionaire_id[0]['target_id'])
      ->execute()->fetchField();
    $questionaire = entityform_empty_load($questionaire_type, 'submit');

    // Let entityform render the form
    module_load_include('inc', 'entityform', 'entityform.admin');

    // language detection
    // We need to find lang code which is stored on survey node
    if (isset($assessment->field_langcode[LANGUAGE_NONE])) {
      $lang_code = $assessment->field_langcode[LANGUAGE_NONE][0]['value'];
      if (isset($lang_code)){
        global $language;
        $installed_languages = language_list();
        $language = $installed_languages[$lang_code];
      }
    }
    if ($token_and_type['type'] == 'token' && isset($assessment->field_sub_tokens_intro_page[LANGUAGE_NONE][0]) && $assessment->field_sub_tokens_intro_page[LANGUAGE_NONE][0]['value'] == 1) { 
    $default_token_welcome_message = i18n_variable_get('assessments_token_submissions_welcome_message', $lang_code);
      $entity_form_output .= '<div class="welcome_message">'.$default_token_welcome_message['value'];
      $entity_form_output .= '<a href="#" id="continue-btn">'.t('Continue').'</a></div>';
      $entity_form_output .= '<div class="entityform-wrapper">';
    }
    $entity_form = entityform_form_wrapper($questionaire, 'submit');
    $entity_form_output .= drupal_render($entity_form);
    $entity_form_output .= '</div>';
    return $entity_form_output;
  }
  else {
    // We did not find an assessment with that token
    return MENU_NOT_FOUND;
  }
}