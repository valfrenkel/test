<?php
/**
 * @file
 * test_users_login_analytics_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function test_users_login_analytics_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
