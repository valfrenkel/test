<?php
/**
 * Generate the admin settings form for analytics pages.
 *
 * @param $form
 *   the form array
 * @param $form_state
 *   the form state array
 * @return mixed
 *   the system settings form
 */
function test_users_login_analytics_views_admin_settings_form($form, &$form_state) {
  $form['test_users_login_analytics_views_pager_items'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('test_users_login_analytics_views_pager_items', ''),
    '#title' => t('Items per analytics pages'),
    '#description' => t('Please enter how many items should be on analytics pages'),
  );

  return system_settings_form($form);
}
