<?php
/**
 * @file
 * test_users_login_analytics_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function test_users_login_analytics_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'analytics';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'login_tracker';
  $view->human_name = 'Analytics';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Analytics: months';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'login_timestamp',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Login tracker: UID */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'login_tracker';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = '';
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  /* Field: Login tracker: Login timestamp */
  $handler->display->display_options['fields']['login_timestamp']['id'] = 'login_timestamp';
  $handler->display->display_options['fields']['login_timestamp']['table'] = 'login_tracker';
  $handler->display->display_options['fields']['login_timestamp']['field'] = 'login_timestamp';
  $handler->display->display_options['fields']['login_timestamp']['label'] = '';
  $handler->display->display_options['fields']['login_timestamp']['exclude'] = TRUE;
  $handler->display->display_options['fields']['login_timestamp']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['login_timestamp']['date_format'] = 'eventsdate';
  $handler->display->display_options['fields']['login_timestamp']['second_date_format'] = 'long';
  /* Sort criterion: Login tracker: Login timestamp */
  $handler->display->display_options['sorts']['login_timestamp']['id'] = 'login_timestamp';
  $handler->display->display_options['sorts']['login_timestamp']['table'] = 'login_tracker';
  $handler->display->display_options['sorts']['login_timestamp']['field'] = 'login_timestamp';
  $handler->display->display_options['sorts']['login_timestamp']['granularity'] = 'day';

  /* Display: Days */
  $handler = $view->new_display('page', 'Days', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Analytics: days';
  $handler->display->display_options['path'] = 'analytics/days';

  /* Display: Months */
  $handler = $view->new_display('page', 'Months', 'page_1');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Login tracker: UID */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'login_tracker';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  /* Field: Login tracker: Login timestamp */
  $handler->display->display_options['fields']['login_timestamp']['id'] = 'login_timestamp';
  $handler->display->display_options['fields']['login_timestamp']['table'] = 'login_tracker';
  $handler->display->display_options['fields']['login_timestamp']['field'] = 'login_timestamp';
  $handler->display->display_options['fields']['login_timestamp']['date_format'] = 'eventsdate';
  $handler->display->display_options['fields']['login_timestamp']['second_date_format'] = 'long';
  $handler->display->display_options['path'] = 'analytics/months';

  /* Display: Weeks */
  $handler = $view->new_display('page', 'Weeks', 'page_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Analytics: weeks';
  $handler->display->display_options['path'] = 'analytics/weeks';
  $export['analytics'] = $view;

  return $export;
}
