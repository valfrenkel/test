<?php
/**
 * @file
 * Code for the TEST Users login analytics views feature.
 */

include_once 'test_users_login_analytics_views.features.inc';
define('DAYS', 'days');
define('WEEKS', 'weeks');
define('MONTHS', 'months');

/**
 * View result to statistics function.
 */
function test_users_login_analytics_views_result_to_statistic ($result, $type) {
  $login_scope = array();
  foreach ($result as $view_row) {
  if (!isset($login_scope[$view_row->login_tracker_login_timestamp_day]) || !in_array($view_row->login_tracker_uid, $login_scope[$view_row->login_tracker_login_timestamp_day])) {
      $login_scope[$view_row->login_tracker_login_timestamp_day][] = $view_row->login_tracker_uid;
    }
  }

  $login_scope = _test_users_login_analytics_get_persent_by_type($login_scope, $type);
  $per_page = variable_get('test_users_login_analytics_views_pager_items', '');
  if(!empty($per_page) && is_numeric($per_page)) {
    $current_page = pager_default_initialize(count($login_scope), $per_page);
    $chunks = array_chunk($login_scope, $per_page, TRUE);
    $output = _test_users_login_analytics_get_theme_by_type($chunks[$current_page], $type);
    $output .= '<br>';
    $output .= theme('pager', array('quantity', count($login_scope)));
  } else {
    $output = _test_users_login_analytics_get_theme_by_type($login_scope, $type);
  }

  return $output;
}

/**
 * Get persent of logins.
 */
function _test_users_login_analytics_get_persent_of_logins($count){
  $count_registered_users = db_select('users', 'u')
    ->condition('u.uid', 1, '!=')
    ->countQuery()
    ->execute()
    ->fetchField();
  return round((($count / $count_registered_users) * 100), 2);
}

/**
 * Get formatted date.
 */
function _test_users_get_formatted_date($date){
  return date('M d Y', $date);
}

/**
 * Get themed date.
 */
function _test_users_get_themed_date($date){
  $output = '<b>';
  $output .= $date;
  $output .= ': ';
  $output .= '</b>';
  return $output;
}

/**
 * Get analytics statistics depends on date type.
 */
function _test_users_login_analytics_get_persent_by_type ($login_scope, $type){
  $output_array = array();
  foreach ($login_scope as $date => $count) {
    if ($type == MONTHS) {
      $date_parsed = substr($date, 0, -2);
      if(isset($output_array[$date_parsed])){
        $output_array[$date_parsed]= sizeof($count) + $output_array[$date_parsed];
      } else {
        $output_array[$date_parsed]= sizeof($count);
      }
    }
    if ($type == DAYS) {
      if(isset($output_array[$date])){
        $output_array[$date]= sizeof($count) + $output_array[$date];
      } else {
        $output_array[$date]= sizeof($count);
      }
    }
    if ($type == WEEKS) {
      $year = date('Y', strtotime($date));
      $week = date('W', strtotime($date));

      if(isset($output_array[$year . $week])){
        $output_array[$year . $week]= sizeof($count) + $output_array[$year . $week];
      } else {
        $output_array[$year . $week]= sizeof($count);
      }
    }

  }
  return $output_array;
}

/**
 * Get analytics theme depends on date type.
 */
function _test_users_login_analytics_get_theme_by_type ($login_scope, $type){
  $output = '';
  foreach ($login_scope as $date => $count) {
    if ($type == MONTHS) {
      $date_parsed = str_split($date, 4);
      $output .= _test_users_get_themed_date(date('F', mktime(0, 0, 0, $date_parsed[1], 10)) . ', ' . $date_parsed[0]);
    }
    if ($type == DAYS) {
      $output .= _test_users_get_themed_date(_test_users_get_formatted_date(strtotime($date)));
    }
    if ($type == WEEKS) {
      $date_parsed = str_split($date, 4);
      $first_week_day = strtotime($date_parsed[0] . 'W' . $date_parsed[1]);
      $last_week_day = strtotime("+6 day", $first_week_day);
      $weeks_output = _test_users_get_formatted_date($first_week_day);
      $weeks_output .= ' - ';
      $weeks_output .= _test_users_get_formatted_date($last_week_day);
      $output .= _test_users_get_themed_date($weeks_output);
    }
    $output .= _test_users_login_analytics_get_persent_of_logins($count) . '%';
    $output .= '<br>';
  }
  return $output;
}

/**
 * Implements hook_menu().
 */
function test_users_login_analytics_views_menu() {
  $items = array();

  $items['admin/config/analytics-settings'] = array(
    'title'            => t('TEST Analytics Settings'),
    'description'      => t('Configure the forms'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('test_users_login_analytics_views_admin_settings_form'),
    'access arguments' => array('access administration pages'),
    'file'             => 'test_users_login_analytics_views.admin.inc',
    'type'             => MENU_NORMAL_ITEM,
    'weight'           => 0,
  );

  $items['admin/reports/analytics'] = array(
    'title'            => t('Users login analytics'),
    'type'             => MENU_NORMAL_ITEM,
    'access arguments' => array('access content'),
    'page callback'    => 'drupal_goto',
    'page arguments'   => array('analytics/days'),
    'weight'           => 0,
  );
  $items['admin/reports/analytics/days'] = array(
    'title'            => ucfirst(DAYS),
    'type'             => MENU_NORMAL_ITEM,
    'access arguments' => array('access content'),
    'page callback'    => 'drupal_goto',
    'page arguments'   => array('analytics/days'),
    'weight'           => 0,
  );
  $items['admin/reports/analytics/weeks'] = array(
    'title'            => ucfirst(WEEKS),
    'type'             => MENU_NORMAL_ITEM,
    'access arguments' => array('access content'),
    'page callback'    => 'drupal_goto',
    'page arguments'   => array('analytics/weeks'),
    'weight'           => 1,
  );
  $items['admin/reports/analytics/months'] = array(
    'title'            => ucfirst(MONTHS),
    'type'             => MENU_NORMAL_ITEM,
    'access arguments' => array('access content'),
    'page callback'    => 'drupal_goto',
    'page arguments'   => array('analytics/months'),
    'weight'           => 2,
  );
  return $items;
}